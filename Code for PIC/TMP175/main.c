#include <p18cxxx.h>
//*********************************************************************************************
#pragma config OSC=HSPLL  // Cristal 10MHz x 4 - 40MHZ/10MIPS
#pragma config IESO=OFF
#pragma config PWRT=ON
#pragma config WDT=OFF
#pragma config MCLRE=ON
#pragma config XINST=OFF
#pragma config DEBUG=OFF
#pragma config FCMEN = OFF
#pragma config LVP=OFF
#pragma config BOREN=OFF
#pragma config PBADEN = OFF
//*********************************************************************************************
#include "TMP175.h"
#include <delays.h>
#include <usart.h>
#include <stdio.h>
#include <string.h>
//*********************************************************************************************
void main(void){
	float Temp;
	unsigned char Enteros,Decimales;

	OpenI2C(MASTER,SLEW_OFF);							// Inicio m�dulo I2C
	SSPADD=99;	 										// Fosc/(4*(SSPxADD+1))
	
	OpenUSART( USART_TX_INT_OFF  &
	           USART_RX_INT_OFF  &
               USART_ASYNCH_MODE &
               USART_EIGHT_BIT   &
               USART_CONT_RX     &
               USART_BRGH_LOW,
               64                );
	
	vSetConfigurationTMP175(SHUTDOWN_MODE_OFF|COMPARATOR_MODE|POLARITY_0|FAULT_QUEUE_6|RESOLUTION_12,0x48); // Address -> A0,A1,A2=0
	vSetTemperatureLowTMP175(25.0);
    vSetTemperatureHighTMP175(35.0);
    Delay10KTCYx(250);
	while(1){
		Delay10KTCYx(250);
		Temp=fReadTemperatureTMP175();
		Enteros=Temp;
		Decimales=(unsigned int)(Temp*10)%10;
		printf("Temp=%d,%01u�C",Enteros,Decimales);
	}
}
