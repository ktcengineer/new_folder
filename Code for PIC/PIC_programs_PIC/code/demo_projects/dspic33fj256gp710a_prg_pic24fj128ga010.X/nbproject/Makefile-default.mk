#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/dspic33fj256gp710a_prg_pic24fj128ga010.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/dspic33fj256gp710a_prg_pic24fj128ga010.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../../prg_lib/icsp.s ../../prg_lib/prg.c ../../prg_lib/ps_39768_39970_70152_30009907_30009934.c main.c pic24fj128ga010_blink.hex.S

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/476307531/icsp.o ${OBJECTDIR}/_ext/476307531/prg.o ${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o ${OBJECTDIR}/main.o ${OBJECTDIR}/pic24fj128ga010_blink.hex.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/476307531/icsp.o.d ${OBJECTDIR}/_ext/476307531/prg.o.d ${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o.d ${OBJECTDIR}/main.o.d ${OBJECTDIR}/pic24fj128ga010_blink.hex.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/476307531/icsp.o ${OBJECTDIR}/_ext/476307531/prg.o ${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o ${OBJECTDIR}/main.o ${OBJECTDIR}/pic24fj128ga010_blink.hex.o

# Source Files
SOURCEFILES=../../prg_lib/icsp.s ../../prg_lib/prg.c ../../prg_lib/ps_39768_39970_70152_30009907_30009934.c main.c pic24fj128ga010_blink.hex.S


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/dspic33fj256gp710a_prg_pic24fj128ga010.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33FJ256GP710A
MP_LINKER_FILE_OPTION=,--script=p33FJ256GP710A.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/476307531/prg.o: ../../prg_lib/prg.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/476307531" 
	@${RM} ${OBJECTDIR}/_ext/476307531/prg.o.d 
	@${RM} ${OBJECTDIR}/_ext/476307531/prg.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../prg_lib/prg.c  -o ${OBJECTDIR}/_ext/476307531/prg.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/476307531/prg.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -I"../../prg_lib" -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/476307531/prg.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o: ../../prg_lib/ps_39768_39970_70152_30009907_30009934.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/476307531" 
	@${RM} ${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o.d 
	@${RM} ${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../prg_lib/ps_39768_39970_70152_30009907_30009934.c  -o ${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -I"../../prg_lib" -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -I"../../prg_lib" -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/476307531/prg.o: ../../prg_lib/prg.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/476307531" 
	@${RM} ${OBJECTDIR}/_ext/476307531/prg.o.d 
	@${RM} ${OBJECTDIR}/_ext/476307531/prg.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../prg_lib/prg.c  -o ${OBJECTDIR}/_ext/476307531/prg.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/476307531/prg.o.d"        -g -omf=elf -I"../../prg_lib" -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/476307531/prg.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o: ../../prg_lib/ps_39768_39970_70152_30009907_30009934.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/476307531" 
	@${RM} ${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o.d 
	@${RM} ${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../prg_lib/ps_39768_39970_70152_30009907_30009934.c  -o ${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o.d"        -g -omf=elf -I"../../prg_lib" -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/476307531/ps_39768_39970_70152_30009907_30009934.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/main.o.d"        -g -omf=elf -I"../../prg_lib" -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/476307531/icsp.o: ../../prg_lib/icsp.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/476307531" 
	@${RM} ${OBJECTDIR}/_ext/476307531/icsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/476307531/icsp.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  ../../prg_lib/icsp.s  -o ${OBJECTDIR}/_ext/476307531/icsp.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -omf=elf -I"../../prg_lib" -Wa,-MD,"${OBJECTDIR}/_ext/476307531/icsp.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_REAL_ICE=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/_ext/476307531/icsp.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/_ext/476307531/icsp.o: ../../prg_lib/icsp.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/476307531" 
	@${RM} ${OBJECTDIR}/_ext/476307531/icsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/476307531/icsp.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  ../../prg_lib/icsp.s  -o ${OBJECTDIR}/_ext/476307531/icsp.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -I"../../prg_lib" -Wa,-MD,"${OBJECTDIR}/_ext/476307531/icsp.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/_ext/476307531/icsp.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/pic24fj128ga010_blink.hex.o: pic24fj128ga010_blink.hex.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/pic24fj128ga010_blink.hex.o.d 
	@${RM} ${OBJECTDIR}/pic24fj128ga010_blink.hex.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  pic24fj128ga010_blink.hex.S  -o ${OBJECTDIR}/pic24fj128ga010_blink.hex.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/pic24fj128ga010_blink.hex.o.d"  -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -omf=elf -I"../../prg_lib" -Wa,-MD,"${OBJECTDIR}/pic24fj128ga010_blink.hex.o.asm.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_REAL_ICE=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/pic24fj128ga010_blink.hex.o.d" "${OBJECTDIR}/pic24fj128ga010_blink.hex.o.asm.d"  -t $(SILENT)  -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/pic24fj128ga010_blink.hex.o: pic24fj128ga010_blink.hex.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/pic24fj128ga010_blink.hex.o.d 
	@${RM} ${OBJECTDIR}/pic24fj128ga010_blink.hex.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  pic24fj128ga010_blink.hex.S  -o ${OBJECTDIR}/pic24fj128ga010_blink.hex.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/pic24fj128ga010_blink.hex.o.d"  -omf=elf -I"../../prg_lib" -Wa,-MD,"${OBJECTDIR}/pic24fj128ga010_blink.hex.o.asm.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/pic24fj128ga010_blink.hex.o.d" "${OBJECTDIR}/pic24fj128ga010_blink.hex.o.asm.d"  -t $(SILENT)  -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/dspic33fj256gp710a_prg_pic24fj128ga010.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/dspic33fj256gp710a_prg_pic24fj128ga010.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -omf=elf -I"../../prg_lib"  -mreserve=data@0x800:0x81F -mreserve=data@0x820:0x821 -mreserve=data@0x822:0x823 -mreserve=data@0x824:0x825 -mreserve=data@0x826:0x84F   -Wl,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_REAL_ICE=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/dspic33fj256gp710a_prg_pic24fj128ga010.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/dspic33fj256gp710a_prg_pic24fj128ga010.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -I"../../prg_lib" -Wl,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}\\xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/dspic33fj256gp710a_prg_pic24fj128ga010.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
