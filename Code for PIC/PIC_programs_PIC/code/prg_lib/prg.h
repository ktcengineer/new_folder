/*************************************************************************
 *  � 2015 Microchip Technology Inc.
 *
 *  Project Name:    PIC programs PIC
 *  FileName:        prg.h
 *  Dependencies:    ps.h
 *  Processor:       PIC24, dsPIC
 *  Compiler:        XC16
 *  IDE:             MPLAB� X
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Description: This file contains a declaration for the top level programmer
 *               function and its return codes.
 *
 *************************************************************************/
/**************************************************************************
 * MICROCHIP SOFTWARE NOTICE AND DISCLAIMER: You may use this software, and
 * any derivatives created by any person or entity by or on your behalf,
 * exclusively with Microchip's products in accordance with applicable
 * software license terms and conditions, a copy of which is provided for
 * your referencein accompanying documentation. Microchip and its licensors
 * retain all ownership and intellectual property rights in the
 * accompanying software and in all derivatives hereto.
 *
 * This software and any accompanying information is for suggestion only.
 * It does not modify Microchip's standard warranty for its products. You
 * agree that you are solely responsible for testing the software and
 * determining its suitability. Microchip has no obligation to modify,
 * test, certify, or support the software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE, ITS INTERACTION WITH
 * MICROCHIP'S PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY
 * APPLICATION.
 *
 * IN NO EVENT, WILL MICROCHIP BE LIABLE, WHETHER IN CONTRACT, WARRANTY,
 * TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), STRICT
 * LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT,
 * SPECIAL, PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE,
 * FOR COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE,
 * HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY
 * OR THE DAMAGES ARE FORESEEABLE. TO THE FULLEST EXTENT ALLOWABLE BY LAW,
 * MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS
 * SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF
 * THESE TERMS.
 *************************************************************************
 *
 * written by Anton Alkhimenok 04/16/2015
 *
 *************************************************************************/

#ifndef _PRG_H_
#define _PRG_H_

#include "ps.h"

/*************************************************************************
 This is a list of return codes for PRGProgramImage(...) function.
 *************************************************************************/
#define PRG_RESULT_CODE_NOT_CONNECTED           1  // device to be programmed in not connected
#define PRG_RESULT_CODE_ERASE_FAILED            2  // failed to erase the device
#define PRG_RESULT_CODE_ROW_PROGRAM_FAIL        3  // failed to program a row in the device
#define PRG_RESULT_CODE_ROW_VERIFICATION_FAIL   4  // source data do not match written
#define PRG_RESULT_CODE_DONE                    0  // programming is successful

/*************************************************************************
 Function:

uint8_t  PRGProgramImage(__prog__ uint16_t* pImage, uint16_t verification)

 Description:

 This function programs an image from internal flash memory to the connected PIC
 device. The image can be created from a HEX file using PIC_programs_PIC utility.
 The image consists of blocks of data. Data size in each block is multiple by
 row size specified in utility GUI (!!! IT MUST MATCH THE DEVICE ROW SIZE SPECIFIED
 IN THE PROGRAMMING SPEC !!!). 
 The image has the following structure:
 -----------------------------------------------------
1st BLOCK ADDRESS (1 instruction word)
1st BLOCK LENGTH  (1 instruction word)
1st BLOCK DATA (BLOCK LENGTH instruction words)

2nd BLOCK ADDRESS (1 instruction word)
2nd BLOCK LENGTH  (1 instruction word)
2nd BLOCK DATA (BLOCK LENGTH instruction words)
 * 
 * 
 * 
 * 
Last BLOCK ADDRESS (1 instruction word)
Last BLOCK LENGTH  (1 instruction word)
Last BLOCK DATA (BLOCK LENGTH instruction words)

0x00FFFFFF (1 instruction word)
-----------------------------------------------------
This programmer function will read all block and program them row by row.

 Parameters:

__prog__ uint16_t* pImage - pointer to the image to be programmed,
uint16_t verification - if non-zero each programmed row will be verified.

 Returned data:

Zero if the programming was successful (PRG_RESULT_CODE_DONE code).

Non zero error code if the programming failed:

PRG_RESULT_CODE_NOT_CONNECTED - device to be programmed in not connected,
PRG_RESULT_CODE_ERASE_FAILED - failed to erase the device,
PRG_RESULT_CODE_ROW_PROGRAM_FAIL - failed to program a row in the device,
PRG_RESULT_CODE_ROW_VERIFICATION_FAIL - source data do not match written.

*************************************************************************/
uint8_t  PRGProgramImage(__prog__ uint16_t* pImage, uint16_t verification);

#endif
