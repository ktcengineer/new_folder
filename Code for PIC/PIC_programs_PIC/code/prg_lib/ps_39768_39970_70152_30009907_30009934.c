/*********************************************************************
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") for its PICmicro Microcontroller is intended and
 * supplied to you, the Company's customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 * 
 * 
 * Written by Anton Alkhimenok 07/06/2015
 * 
 *********************************************************************/

#include "ps.h"

#define  ROW_SIZE     64    // in 24-bit instructions

uint16_t PSGetRowSize()
{
    return ROW_SIZE;
}

uint8_t PSConnect()
{
uint16_t result = -1;

   ICSPConnect();

   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x040200); // GOTO #0x0200
   ICSPWriteCMD(0x000000);

   ICSPWriteCMD(0x255552); // MOV #0x5555, W2
   ICSPWriteCMD(0x883C22); // MOV W2, VISI
   ICSPWriteCMD(0x000000);

   result = ICSPReadVISI();

   if(result != 0x5555)
   {
       return 1;
   }
   return 0;
}

uint8_t PSPollWR()
{
uint16_t result = -1;
uint16_t timeout = WR_TIMEOUT;

   // program
   ICSPWriteCMD(0xA8E761); // BSET NVMCON, #WR
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

    // poll WR bit to supply enough clock cycles for the programming process
   while(result != 0)
   {
       ICSPWriteCMD(0x803B02); // MOV NVMCON, W2
       ICSPWriteCMD(0x883C22); // MOV W2, VISI
       ICSPWriteCMD(0x000000);

       result = ICSPReadVISI();
       result &= 0x8000; // mask WR bit

       if(timeout == 0)
       {
           return 1;
       }
       timeout--;

       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x040200); // GOTO #0x0200
       ICSPWriteCMD(0x000000);
   }
   return 0;
}

uint32_t PSRead(uint32_t address)
{ 
UINT32UNION result;
uint32_t opcode = 0;

   opcode = (address>>12)&0x000FF0;
   opcode |= 0x200000;
   ICSPWriteCMD(opcode); // MOV #Address<23:16>, W0

   ICSPWriteCMD(0x207847); // MOV #VISI, W7

   ICSPWriteCMD(0x880190); // MOV W0, TBLPAG

   opcode = (address<<4)&0x0FFFF0;
   opcode |= 0x200006;
   ICSPWriteCMD(opcode); // MOV #Address<15:0>, W6
   
   ICSPWriteCMD(0x000000);   
   ICSPWriteCMD(0x040200); // GOTO #0x0200
   ICSPWriteCMD(0x000000);

   ICSPWriteCMD(0xBA8B96); // TBLRDH [W6],[W7]
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

   result.word16hi = ICSPReadVISI();

   ICSPWriteCMD(0xBA0BB6); // TBLRDL [W6++],[W7]
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

   result.word16lo = ICSPReadVISI();

   return result.word32;
}

uint8_t PSWrite(uint32_t address, uint32_t data)
{
uint32_t opcode = 0;

   opcode = (address>>12)&0x000FF0;
   opcode |= 0x200000;
   ICSPWriteCMD(opcode);   // MOV #Address<23:16>, W0
   ICSPWriteCMD(0x880190); // MOV W0, TBLPAG

   opcode = (address<<4)&0x0FFFF0;
   opcode |= 0x200006;
   ICSPWriteCMD(opcode); // MOV #Address<15:0>, W6

   ICSPWriteCMD(0x24003A); // MOV #0x4003, W10
   ICSPWriteCMD(0x883B0A); // MOV W10, NVMCON
   
   opcode = (data<<4)&0x0FFFF0;
   opcode |= 0x200000;
   ICSPWriteCMD(opcode); // MOV #word<15:0>, W0

   opcode = (data>>12)&0x0FFFF0;
   opcode |= 0x200001;
   ICSPWriteCMD(opcode); // MOV #word<15:0>, W1

   ICSPWriteCMD(0xBB0B00); // TBLWTL W0,[W6]
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

   ICSPWriteCMD(0xBB9B01); // TBLWTH W1,[W6++]
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

   if(PSPollWR())
   {
       return 1;
   }

   return 0;
}

void PSReadRow(uint32_t address, uint32_t* pData)
{ 
UINT32UNION result;
uint32_t opcode;
uint32_t count = ROW_SIZE;
   
   opcode = (address>>12)&0x000FF0;
   opcode |= 0x200000;
   ICSPWriteCMD(opcode); // MOV #Address<23:16>, W0

   ICSPWriteCMD(0x207847); // MOV #VISI, W7

   ICSPWriteCMD(0x880190); // MOV W0, TBLPAG

   opcode = (address<<4)&0x0FFFF0;
   opcode |= 0x200006;
   ICSPWriteCMD(opcode); // MOV #Address<15:0>, W6
   ICSPWriteCMD(0x000000);
   
   while(count--)
   {       
       ICSPWriteCMD(0x040200); // GOTO #0x0200
       ICSPWriteCMD(0x000000);

       ICSPWriteCMD(0xBA8B96); // TBLRDH [W6],[W7]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);

       result.word16hi = ICSPReadVISI();

       ICSPWriteCMD(0xBA0BB6); // TBLRDL [W6++],[W7]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);

       result.word16lo = ICSPReadVISI();

       *pData++ = result.word32;
   }
}

uint8_t PSWriteRow(uint32_t address, uint32_t* pData)
{
uint32_t opcode = 0;
uint32_t data;
uint16_t count = ROW_SIZE;

   opcode = (address>>12)&0x000FF0;
   opcode |= 0x200000;
   ICSPWriteCMD(opcode);   // MOV #Address<23:16>, W0
   ICSPWriteCMD(0x880190); // MOV W0, TBLPAG

   opcode = (address<<4)&0x0FFFF0;
   opcode |= 0x200006;
   ICSPWriteCMD(opcode); // MOV #Address<15:0>, W6

   ICSPWriteCMD(0x24001A); // MOV #0x4001, W10
   ICSPWriteCMD(0x883B0A); // MOV W10, NVMCON

   while(count--)
   {
       data = *pData++;
       opcode = (data<<4)&0x0FFFF0;
       opcode |= 0x200000;
       ICSPWriteCMD(opcode); // MOV #word<15:0>, W0

       opcode = (data>>12)&0x0FFFF0;
       opcode |= 0x200001;
       ICSPWriteCMD(opcode); // MOV #word<15:0>, W1

       ICSPWriteCMD(0xBB0B00); // TBLWTL W0,[W6]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);

       ICSPWriteCMD(0xBB9B01); // TBLWTH W1,[W6++]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
   }

   if(PSPollWR())
   {
       return 1;
   }
   return 0;
}

uint8_t PSErase()
{
   // erase flash

   ICSPWriteCMD(0x2404FA); // MOV #0x404F, W10
   ICSPWriteCMD(0x883B0A); // MOV W10, NVMCON

   ICSPWriteCMD(0x200000); // MOV #0x0000, W0
   ICSPWriteCMD(0x880190); // MOV W0, TBLPAG
   ICSPWriteCMD(0xBB0800); // TBLWTL [W0], W0
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

   if(PSPollWR())
   {
       return 1;
   }

   return 0;
}

