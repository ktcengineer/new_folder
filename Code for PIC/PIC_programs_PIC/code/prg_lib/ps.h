/*************************************************************************
 *  � 2015 Microchip Technology Inc.
 *
 *  Project Name:    PIC programs PIC
 *  FileName:        ps.h
 *  Dependencies:    icsp.h
 *  Processor:       PIC24, dsPIC
 *  Compiler:        XC16
 *  IDE:             MPLAB� X
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Description: This file contains the declarations for the programming
 *               procedures defined in the programming specification.
 *
 *************************************************************************/
/**************************************************************************
 * MICROCHIP SOFTWARE NOTICE AND DISCLAIMER: You may use this software, and
 * any derivatives created by any person or entity by or on your behalf,
 * exclusively with Microchip's products in accordance with applicable
 * software license terms and conditions, a copy of which is provided for
 * your referencein accompanying documentation. Microchip and its licensors
 * retain all ownership and intellectual property rights in the
 * accompanying software and in all derivatives hereto.
 *
 * This software and any accompanying information is for suggestion only.
 * It does not modify Microchip's standard warranty for its products. You
 * agree that you are solely responsible for testing the software and
 * determining its suitability. Microchip has no obligation to modify,
 * test, certify, or support the software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE, ITS INTERACTION WITH
 * MICROCHIP'S PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY
 * APPLICATION.
 *
 * IN NO EVENT, WILL MICROCHIP BE LIABLE, WHETHER IN CONTRACT, WARRANTY,
 * TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), STRICT
 * LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT,
 * SPECIAL, PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE,
 * FOR COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE,
 * HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY
 * OR THE DAMAGES ARE FORESEEABLE. TO THE FULLEST EXTENT ALLOWABLE BY LAW,
 * MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS
 * SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF
 * THESE TERMS.
 *************************************************************************
 *
 * written by Anton Alkhimenok 04/16/2015
 *
 *************************************************************************/

#ifndef _PS_H_
#define _PS_H_

#include "icsp.h"


/*************************************************************************
 This parameter defines a timeout for WRTIE and ERASE operations.
 *************************************************************************/
#define  WR_TIMEOUT   8000

/*************************************************************************
 Function:

uint8_t PSConnect()

 Description:

 This function tries to connect to device programmed and verifies the connection.

 Parameters:

 None.
 
 Returned data:

 Zero if the device is successfully connected. Non-zero if there's no connection.
*************************************************************************/
uint8_t PSConnect();

/*************************************************************************
 Macro:

 void PSDisconnect()

 Description:

 This macro resets and releases the programmed PIC device.

 Parameters:

 None.

 Returned data:

 None.
*************************************************************************/
#define PSDisconnect()  ICSPDisconnect()

/*************************************************************************
 Function:

uint32_t PSRead(uint32_t address)

 Description:

 This function reads one word from PIC flash. The device must be connected
 with uint8_t PCConnect() function first.

 Parameters:

 uint32_t address - start address.
  
 Returned data:

 Data read.
 
*************************************************************************/
uint32_t PSRead(uint32_t address);

/*************************************************************************
 Function:

 uint8_t PSWrite(uint32_t address, uint32_t data)

 Description:

 This function programs one word to PIC flash. Before programming
 the device must be connected with uint8_t PSConnect() function and erased using
 uint8_t PSErase() function.
 !!! May not be implemented for some devices !!! See programming specification
 to verify that the word programming is available.

 Parameters:

 uint32_t address - start address of the data,
 uint32_t data - data to be written.
 
 Returned data:

Zero if the programming was successful. Non zero if error occurs.
*************************************************************************/
uint8_t PSWrite(uint32_t address, uint32_t data);

/*************************************************************************
 Function:

void PSReadRow(uint32_t address, uint32_t* pData)

 Description:

 This function reads one row from PIC flash. See programming specification
 to find out the row size.  The device must be connected with uint8_t PCConnect()
 function first.

 Parameters:

 uint32_t address - start address of the row,
 uint32_t* pData - pointer to array to store the row data.
 
 Returned data:

 None.
*************************************************************************/
void PSReadRow(uint32_t address, uint32_t* pData);

/*************************************************************************
 Function:

 uint8_t PSWriteRow(uint32_t address, uint32_t* pData)

 Description:

 This function programs one row of PIC flash. See programming specification
 to find out the row size. Before programming the device must be connected
 with uint8_t PSConnect() function and erased using uint8_t PSErase() function.

 Parameters:

 uint32_t address - start address of the row,
 uint32_t* pData - pointer to array with data.
 
 Returned data:

Zero if the programming was successful. Non zero if error occurs.
*************************************************************************/
uint8_t PSWriteRow(uint32_t address, uint32_t* pData);

/*************************************************************************
 Function:

uint8_t PSErase()

 Description:

 This function erases code flash.  The device must be connected
 with uint8_t PCConnect() function first.

 Parameters:

 None.
 
 Returned data:

Zero if the erase operation was successful. Non zero if error occurs.
*************************************************************************/
uint8_t PSErase();

/*************************************************************************
 Function:

uint16_t PSGetRowSize()

 Description:

 This function returns a row size in instruction words for the flash memory
 of the programmed PIC device.

 Parameters:

 None.
 
 Returned data:

 This function returns a row size in instruction words for the flash memory
 of the programmed PIC device.
*************************************************************************/
uint16_t PSGetRowSize();


typedef union {
    uint32_t word32;
    struct
    {
        uint16_t word16lo;
        uint16_t word16hi;
    };
} UINT32UNION;

#endif