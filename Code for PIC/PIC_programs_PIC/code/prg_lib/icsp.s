/*********************************************************************
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") for its PICmicro Microcontroller is intended and
 * supplied to you, the Company's customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 * 
 * 
 * Written by Anton Alkhimenok 07/06/2015
 * 
 *********************************************************************/
 
.include "icsp.inc"


.if PGC_DELAY_CYCLES == 0
.macro PGC_DELAY
.endm
.else

.if PGC_DELAY_CYCLES == 1
.macro PGC_DELAY
    nop
.endm
.else
.macro PGC_DELAY
    repeat #(PGC_DELAY_CYCLES-2)
    nop
.endm
.endif
.endif

    .global _ICSPDelay100uS
    .global _ICSPWriteCMD
    .global _ICSPReadVISI
    .global _ICSPConnect
    .global _ICSPDisconnect

    .section .text

_ICSPDelay100uS:
    repeat #(CPU_INSTRUCTION_FREQUENCY/10000 - 2)
    nop
    dec w0, w0
    bra nz, _ICSPDelay100uS
    return

_ICSPWriteCMD:
    ; the 24-bit command is in w0/w1 registers

    ; transmit 0000 on PGD
    bclr PGD_LAT_REGISTER, #PGD_BIT_NUMBER

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    mov  #16, w3
tx16bits:
    ; set PGD
    btsc w0, #0
    bset PGD_LAT_REGISTER, #PGD_BIT_NUMBER
    btss w0, #0
    bclr PGD_LAT_REGISTER, #PGD_BIT_NUMBER
    ; generate PGC pulse
    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    ; shift to the next bit
    lsr  w0, #1, w0
    ; decrement counter
    dec w3, w3
    bra nz, tx16bits

    mov  #8, w3
tx8bits:
    ; set PGD
    btsc w1, #0
    bset PGD_LAT_REGISTER, #PGD_BIT_NUMBER
    btss w1, #0
    bclr PGD_LAT_REGISTER, #PGD_BIT_NUMBER
    ; generate PGC pulse
    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    ; shift to the next bit
    lsr  w1, #1, w1
    ; decrement counter
    dec w3, w3
    bra nz, tx8bits

    return

_ICSPReadVISI:

    ; transmit 1000 on PGD
    bset PGD_LAT_REGISTER, #PGD_BIT_NUMBER

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bclr PGD_LAT_REGISTER, #PGD_BIT_NUMBER

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    ; transmit 0000 0000 on PGD
    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER

    ; switch PGD to input
    bset PGD_TRIS_REGISTER, #PGD_BIT_NUMBER
    PGC_DELAY

    ; load 16 bits from VISI to w0
    mov  #16, w3
    clr w0
rx16bits:
    ; shift to the next bit
    lsr  w0, #1, w0
    ; get bit
    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    btsc PGD_PORT_REGISTER, #PGD_BIT_NUMBER
    bset w0, #15
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    ; decrement counter
    dec w3, w3
    bra nz, rx16bits

    ; switch PGD to output
    bclr PGD_TRIS_REGISTER, #PGD_BIT_NUMBER

    return

_ICSPConnect:

    ; initialize PGC, PGD and MCLR I/Os
    bclr PGD_LAT_REGISTER, #PGD_BIT_NUMBER
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    bclr MCLR_LAT_REGISTER, #MCLR_BIT_NUMBER
    bclr PGD_TRIS_REGISTER, #PGD_BIT_NUMBER
    bclr PGC_TRIS_REGISTER, #PGC_BIT_NUMBER
    bclr MCLR_TRIS_REGISTER, #MCLR_BIT_NUMBER

    ; wait approx. 100 x 0.1 mS = 10 mS
    mov   #100, w0
    call _ICSPDelay100uS

    ; set MCLR high
    bset MCLR_LAT_REGISTER, #MCLR_BIT_NUMBER

    ; wait approx. 500 uS
    mov   #5, w0
    call _ICSPDelay100uS

    ; set MCLR low
    bclr MCLR_LAT_REGISTER, #MCLR_BIT_NUMBER

    ; wait approx. 20 x 0.1 mS = 2 mS
    mov   #20, w0
    call _ICSPDelay100uS

    ; send ICSP entry code 0x4D434851 MSB first
    mov  #0x4D43, w0
    mov  #16, w3
codehigh:
    ; set PGD
    btsc w0, #15
    bset PGD_LAT_REGISTER, #PGD_BIT_NUMBER
    btss w0, #15
    bclr PGD_LAT_REGISTER, #PGD_BIT_NUMBER
    ; generate PGC pulse
    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    ; shift to the next bit
    sl  w0, #1, w0
    ; decrement counter
    dec w3, w3
    bra nz, codehigh

    mov  #0x4851, w0
    mov  #16, w3
codelow:
    ; set PGD
    btsc w0, #15
    bset PGD_LAT_REGISTER, #PGD_BIT_NUMBER
    btss w0, #15
    bclr PGD_LAT_REGISTER, #PGD_BIT_NUMBER
    ; generate PGC pulse
    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    ; shift to the next bit
    sl  w0, #1, w0
    ; decrement counter
    dec w3, w3
    bra nz, codelow

    ; wait approx. 20 x 0.1 mS = 2 mS
    mov   #20, w0
    call _ICSPDelay100uS

    ; set MCLR high
    bset MCLR_LAT_REGISTER, #MCLR_BIT_NUMBER

    ; wait approx. 540 x 0.1 mS = 54 mS
    mov   #540, w0
    call _ICSPDelay100uS

    ; transmit 00000 on PGD
    bclr PGD_LAT_REGISTER, #PGD_BIT_NUMBER

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    bset PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY
    bclr PGC_LAT_REGISTER, #PGC_BIT_NUMBER
    PGC_DELAY

    return


_ICSPDisconnect:

    ; reset
    bclr MCLR_LAT_REGISTER, #MCLR_BIT_NUMBER
    ; wait approx. 5 mS
    mov   #50, w0
    call _ICSPDelay100uS
    bset MCLR_LAT_REGISTER, #MCLR_BIT_NUMBER

    bset PGD_TRIS_REGISTER, #PGD_BIT_NUMBER
    bset PGC_TRIS_REGISTER, #PGC_BIT_NUMBER
    bset MCLR_TRIS_REGISTER, #MCLR_BIT_NUMBER

    return

    .end
