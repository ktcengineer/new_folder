/*************************************************************************
 *  � 2015 Microchip Technology Inc.
 *
 *  Project Name:    PIC programs PIC
 *  FileName:        icsp.h
 *  Dependencies:    xc.inc, xc.h, stdint.h
 *  Processor:       PIC24, dsPIC
 *  Compiler:        XC16
 *  IDE:             MPLAB� X
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Description: This file contains the programmer I/O and timing settings
 *               and declarations for the basic ICSP functions.
 *
 *************************************************************************/
/**************************************************************************
 * MICROCHIP SOFTWARE NOTICE AND DISCLAIMER: You may use this software, and
 * any derivatives created by any person or entity by or on your behalf,
 * exclusively with Microchip's products in accordance with applicable
 * software license terms and conditions, a copy of which is provided for
 * your referencein accompanying documentation. Microchip and its licensors
 * retain all ownership and intellectual property rights in the
 * accompanying software and in all derivatives hereto.
 *
 * This software and any accompanying information is for suggestion only.
 * It does not modify Microchip's standard warranty for its products. You
 * agree that you are solely responsible for testing the software and
 * determining its suitability. Microchip has no obligation to modify,
 * test, certify, or support the software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE, ITS INTERACTION WITH
 * MICROCHIP'S PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY
 * APPLICATION.
 *
 * IN NO EVENT, WILL MICROCHIP BE LIABLE, WHETHER IN CONTRACT, WARRANTY,
 * TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), STRICT
 * LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT,
 * SPECIAL, PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE,
 * FOR COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE,
 * HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY
 * OR THE DAMAGES ARE FORESEEABLE. TO THE FULLEST EXTENT ALLOWABLE BY LAW,
 * MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS
 * SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF
 * THESE TERMS.
 *************************************************************************
 *
 * written by Anton Alkhimenok 04/16/2015
 *
 *************************************************************************/
#ifndef _ICSP_H_
#define _ICSP_H_

#include <xc.h>
#include <stdint.h>

/*************************************************************************
 Function:

 void ICSPDelay100uS(uint16_t delay)

 Description:

 This function inserts a delay in the code execution. It depends on
 CPU_INSTRUCTION_FREQUENCY parameter.

 Parameters:

 uint16_t delay - delay in 100 uS steps.

 Returned data:

 None.
*************************************************************************/
extern void ICSPDelay100uS(uint16_t delay);

/*************************************************************************
 Macro:

 void ICSPDelay1mS(uint16_t delay)

 Description:

 This macro inserts a delay in the code execution. It depends on
 CPU_INSTRUCTION_FREQUENCY parameter.

 Parameters:

 uint16_t delay - delay in 1 mS steps (!!! 6553 mS maximum !!!).

 Returned data:

 None.
*************************************************************************/
#define ICSPDelay1mS(delay) ICSPDelay100uS(10*delay);


/*************************************************************************
 Function:

 void ICSPWriteCMD(uint32_t opcode)

 Description:

 This function executes a 24-bit instruction on the programmed PIC device
 via ICSP interface. ICSPConnect() function must be called first to start
 ICSP communication.

 Parameters:

 uint32_t opcode - 24-bit instruction opcode to be executed.

 Returned data:

 None.
*************************************************************************/
extern void ICSPWriteCMD(uint32_t opcode);

/*************************************************************************
 Function:

 uint16_t ICSPReadVISI()

 Description:

 This function reads VISI register from the programmed PIC device via
 ICSP interface. ICSPConnect() function must be called first to start
 ICSP communication.

 Parameters:

 None.

 Returned data:

 VISI register value.
*************************************************************************/
extern uint16_t ICSPReadVISI();


/*************************************************************************
 Function:

 void ICSPConnect()

 Description:

 This function initialize I/Os used for ICSP interface and enters in
 ICSP mode on the programmed PIC device.

 Parameters:

 None.

 Returned data:

 None.
*************************************************************************/
extern void ICSPConnect();

/*************************************************************************
 Function:

 void ICSPDisconnect()

 Description:

 This function resets and releases the programmed PIC device, disconnects
 I/Os used for ICSP communication.

 Parameters:

 None.

 Returned data:

 None.
*************************************************************************/
extern void ICSPDisconnect();

#endif
