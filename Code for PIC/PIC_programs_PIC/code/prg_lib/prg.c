/*********************************************************************
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") for its PICmicro Microcontroller is intended and
 * supplied to you, the Company's customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 * 
 * 
 * Written by Anton Alkhimenok 07/06/2015
 * 
 *********************************************************************/

#include "prg.h"

typedef struct PROG_POINTER_TAG {
  union {
    __prog__ uint16_t* pointer;
    struct {
        uint16_t offset;
        uint8_t  tlbpag;
    };
  };
} PROG_POINTER;


uint32_t PRGReadFlash(__prog__ uint16_t* pData)
{
UINT32UNION  value;    
PROG_POINTER address;

    address.pointer = pData;
    TBLPAG = address.tlbpag;
    value.word16hi = __builtin_tblrdh(address.offset);
    value.word16lo = __builtin_tblrdl(address.offset);    
    
    return value.word32;
}

uint8_t  PRGProgramImage(__prog__ uint16_t* pImage, uint16_t verification)
{
uint32_t address;
int32_t  count;
uint16_t flashRowSize;
uint32_t* pBufferWrite;
uint32_t* pBufferRead;
uint16_t i;

    flashRowSize = PSGetRowSize();

// create buffers in stack
uint32_t bufferWrite[64];//flashRowSize];
uint32_t bufferRead[64];//flashRowSize];

    if(PSConnect())
    {
        return PRG_RESULT_CODE_NOT_CONNECTED;    
    }

    if(PSErase())
    {
        return PRG_RESULT_CODE_ERASE_FAILED;    
    }
    
    // read and program all blocks for the image
    while(1)
    {
        address = PRGReadFlash(pImage++);
        
        // check if all data are programmed
        if(address == 0x00FFFFFF)
        {
            break;
        }
        
        count = PRGReadFlash(pImage++);
                
        while(count > 0)
        {
            // load row
            pBufferWrite = bufferWrite;
            for(i = 0; i < flashRowSize; i++)
            {
                *pBufferWrite++ = PRGReadFlash(pImage++);
            }

            // program row
            if(PSWriteRow(address, bufferWrite))
            {
                return PRG_RESULT_CODE_ROW_PROGRAM_FAIL;
            }
            
            if(verification)
            {
                // read row
                PSReadRow(address, bufferRead);
            
                // verify row
                pBufferWrite = bufferWrite;
                pBufferRead = bufferRead;            
                for(i = 0; i < flashRowSize; i++)
                {
                    if(*pBufferWrite++ != *pBufferRead++)
                    {
                        return PRG_RESULT_CODE_ROW_VERIFICATION_FAIL;                    
                    }
                }
            }
            
            count -= flashRowSize;
            address += (flashRowSize<<1);
        }    
    }

    PSDisconnect();
    
    return PRG_RESULT_CODE_DONE;
}