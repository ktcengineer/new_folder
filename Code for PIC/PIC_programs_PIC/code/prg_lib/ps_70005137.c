/*********************************************************************
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") for its PICmicro Microcontroller is intended and
 * supplied to you, the Company's customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 * 
 * 
 * Written by Anton Alkhimenok 07/06/2015
 * 
 *********************************************************************/

#include "ps.h"

#define  ROW_SIZE     64    // in 24-bit instructions

uint16_t PSGetRowSize()
{
    return ROW_SIZE;
}

uint8_t PSConnect()
{
uint16_t result = -1;

   ICSPConnect();

   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x040200); // GOTO #0x0200
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

   ICSPWriteCMD(0x255552); // MOV #0x5555, W2
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x887C42); // MOV W2, VISI
   ICSPWriteCMD(0x000000);

   result = ICSPReadVISI();

   if(result != 0x5555)
   {
       return 1;
   }
   return 0;
}

uint8_t PSPollWR()
{
uint16_t result = -1;
uint16_t timeout = WR_TIMEOUT;

   // program
   ICSPWriteCMD(0x200551); // MOV 0x5555, W1
   ICSPWriteCMD(0x883971); // MOV W1, NVMKEY
   ICSPWriteCMD(0x200AA1); // MOV 0xAAAA, W1
   ICSPWriteCMD(0x883971); // MOV W1, NVMKEY
   ICSPWriteCMD(0xA8E729); // BSET NVMCON, #WR
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

    // poll WR bit to supply enough clock cycles for the programming process
   while(result != 0)
   {
       ICSPWriteCMD(0x803942); // MOV NVMCON, W2
       ICSPWriteCMD(0x000000);       
       ICSPWriteCMD(0x887C42); // MOV W2, VISI
       ICSPWriteCMD(0x000000);

       result = ICSPReadVISI();
       Nop();
       Nop();
       Nop();
       Nop();
       result &= 0x8000; // mask WR bit

       if(timeout == 0)
       {
           return 1;
       }
       timeout--;

       ICSPWriteCMD(0x040200); // GOTO #0x0200
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
   }
   return 0;
}

uint32_t PSRead(uint32_t address)
{ 
UINT32UNION result;
uint32_t opcode = 0;

   ICSPWriteCMD(0x20F887); // MOV #VISI, W7

   opcode = (address<<4)&0x0FFFF0;
   opcode |= 0x200006;
   ICSPWriteCMD(opcode); // MOV #Address<15:0>, W6
   ICSPWriteCMD(0x000000);
   
   opcode = (address>>12)&0x000FF0;
   opcode |= 0x200000;
   ICSPWriteCMD(opcode); // MOV #Address<23:16>, W0
   ICSPWriteCMD(0x8802A0); // MOV W0, TBLPAG
   address += 2;

   ICSPWriteCMD(0x040200); // GOTO #0x0200
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

   ICSPWriteCMD(0xBA8B96); // TBLRDH [W6],[W7]
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

   result.word16hi = ICSPReadVISI();

   ICSPWriteCMD(0xBA0BB6); // TBLRDL [W6++],[W7]
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);
   ICSPWriteCMD(0x000000);

   result.word16lo = ICSPReadVISI();

   return result.word32;
}

void PSReadRow(uint32_t address, uint32_t* pData)
{ 
UINT32UNION result;
uint32_t opcode = 0;
uint16_t count = ROW_SIZE;

   ICSPWriteCMD(0x20F887); // MOV #VISI, W7

   opcode = (address<<4)&0x0FFFF0;
   opcode |= 0x200006;
   ICSPWriteCMD(opcode); // MOV #Address<15:0>, W6
   ICSPWriteCMD(0x000000);
   
   while(count--)
   {
       opcode = (address>>12)&0x000FF0;
       opcode |= 0x200000;
       ICSPWriteCMD(opcode); // MOV #Address<23:16>, W0
       ICSPWriteCMD(0x8802A0); // MOV W0, TBLPAG
       address += 2;

       ICSPWriteCMD(0x040200); // GOTO #0x0200
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);

       ICSPWriteCMD(0xBA8B96); // TBLRDH [W6],[W7]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);

       result.word16hi = ICSPReadVISI();

       ICSPWriteCMD(0xBA0BB6); // TBLRDL [W6++],[W7]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);

       result.word16lo = ICSPReadVISI();

       *pData++ = result.word32;
   }
}

uint8_t PSWriteRow(uint32_t address, uint32_t* pData)
{
uint32_t opcode = 0;
uint32_t lowWord, highWord;
uint16_t count = ROW_SIZE;

   ICSPWriteCMD(0x24001A); // MOV #0x4001, W10
   ICSPWriteCMD(0x200FAC); // MOV #0x00FA, W12
   ICSPWriteCMD(0x88394A); // MOV W10, NVMCON   
   ICSPWriteCMD(0x8802AC); // MOV W12, TBLPAG

   while(count > 0)
   {
       count -= 2;

       ICSPWriteCMD(0xEB0300); // CLR W6

       // write latches
       lowWord = *pData++;
       highWord = *pData++;

       opcode = (lowWord<<4)&0x0FFFF0;
       opcode |= 0x200000;
       ICSPWriteCMD(opcode); // MOV #lowWord<15:0>, W0

       opcode = (lowWord>>12)&0x0FFFF0;
       opcode |= 0x200001;
       ICSPWriteCMD(opcode); // MOV #lowWord<23:16>, W1

       opcode = (highWord<<4)&0x0FFFF0;
       opcode |= 0x200002;
       ICSPWriteCMD(opcode); // MOV #highWord<15:0>, W2

       opcode = (highWord>>12)&0x0FFFF0;
       opcode |= 0x200003;
       ICSPWriteCMD(opcode); // MOV #highWord<23:16>, W3

       ICSPWriteCMD(0xBB0B00); // TBLWTL W0,[W6]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);

       ICSPWriteCMD(0xBB9B01); // TBLWTH W1,[W6++]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);

       ICSPWriteCMD(0xBB0B02); // TBLWTL W2,[W6]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);

       ICSPWriteCMD(0xBB9B03); // TBLWTH W3,[W6++]
       ICSPWriteCMD(0x000000);
       ICSPWriteCMD(0x000000);


       // set address
       opcode = (address<<4)&0x0FFFF0;
       opcode |= 0x200004;
       ICSPWriteCMD(opcode); // MOV #Address<15:0>, W4

       opcode = (address>>12)&0x0FFFF0;
       opcode |= 0x200005;
       ICSPWriteCMD(opcode); // MOV #Address<23:16>, W5

       ICSPWriteCMD(0x883954); // MOV W4, NVMADDR
       ICSPWriteCMD(0x883965); // MOV W5, NVMADDRU

       address += 4;

       if(PSPollWR())
       {
           return 1;
       }       
   }
   return 0;
}

uint8_t PSErase()
{
   // erase flash
   ICSPWriteCMD(0x2400EA); // MOV #0x400E, W10
   ICSPWriteCMD(0x88394A); // MOV W10, NVMCON
   ICSPWriteCMD(0x000000);

   if(PSPollWR())
   {
       return 1;
   }
   ICSPDelay100uS(500);
   return 0;
}

