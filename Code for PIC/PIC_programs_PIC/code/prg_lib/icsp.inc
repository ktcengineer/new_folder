/*************************************************************************
 *  � 2015 Microchip Technology Inc.
 *
 *  Project Name:    PIC programs PIC
 *  FileName:        icsp.h
 *  Dependencies:    xc.inc, xc.h, stdint.h
 *  Processor:       PIC24, dsPIC
 *  Compiler:        XC16
 *  IDE:             MPLAB� X
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Description: This file contains the programmer I/O and timing settings
 *               and declarations for the basic ICSP functions.
 *
 *************************************************************************/
/**************************************************************************
 * MICROCHIP SOFTWARE NOTICE AND DISCLAIMER: You may use this software, and
 * any derivatives created by any person or entity by or on your behalf,
 * exclusively with Microchip's products in accordance with applicable
 * software license terms and conditions, a copy of which is provided for
 * your referencein accompanying documentation. Microchip and its licensors
 * retain all ownership and intellectual property rights in the
 * accompanying software and in all derivatives hereto.
 *
 * This software and any accompanying information is for suggestion only.
 * It does not modify Microchip's standard warranty for its products. You
 * agree that you are solely responsible for testing the software and
 * determining its suitability. Microchip has no obligation to modify,
 * test, certify, or support the software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE, ITS INTERACTION WITH
 * MICROCHIP'S PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY
 * APPLICATION.
 *
 * IN NO EVENT, WILL MICROCHIP BE LIABLE, WHETHER IN CONTRACT, WARRANTY,
 * TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), STRICT
 * LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT,
 * SPECIAL, PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE,
 * FOR COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE,
 * HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY
 * OR THE DAMAGES ARE FORESEEABLE. TO THE FULLEST EXTENT ALLOWABLE BY LAW,
 * MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS
 * SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF
 * THESE TERMS.
 *************************************************************************
 *
 * written by Anton Alkhimenok 04/16/2015
 *
 *************************************************************************/

.include "xc.inc"

/*************************************************************************
 This parameter is a CPU instruction frequency in Hz.
 It defines timing for ICSP interface.
 ICSP may not work if this parameter is not correct.
 !!! This frequency is a half of the system oscillator frequency !!!
 *************************************************************************/
.equ    CPU_INSTRUCTION_FREQUENCY,  40000000

/*************************************************************************
 This parameters indentify the I/O used as PGD signal.
 *************************************************************************/
.equ    PGD_TRIS_REGISTER,          TRISA
.equ    PGD_LAT_REGISTER,           LATA
.equ    PGD_PORT_REGISTER,          PORTA
.equ    PGD_BIT_NUMBER,             5

/*************************************************************************
 This parameters indentify the I/O used as ICSP PGC signal.
 *************************************************************************/
.equ    PGC_TRIS_REGISTER,          TRISA
.equ    PGC_LAT_REGISTER,           LATA
.equ    PGC_BIT_NUMBER,             0

/*************************************************************************
 This parameters indentify the I/O used as ICSP MCLR signal.
 *************************************************************************/
.equ    MCLR_TRIS_REGISTER,         TRISA
.equ    MCLR_LAT_REGISTER,          LATA
.equ    MCLR_BIT_NUMBER,            4

/*************************************************************************
 This parameter defines a minimum delay in instruction cycles
 between transitions of the ICSP PGC signal.
 Usually for the reliable communication the ICSP PGC clock frequency must
 not exceed 5 MHz.
 !!! Increase this delay to slow down the PGC clock/increase programming
     time if ICSP communication is not relaible. !!!
 *************************************************************************/
.equ    PGC_DELAY_CYCLES,           1
