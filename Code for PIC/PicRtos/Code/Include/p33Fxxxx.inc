;;
;; generic include file for dsPIC33F
;;

.ifdef __33FJ06GS101
.include "p33FJ06GS101.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ06GS102
.include "p33FJ06GS102.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ06GS202
.include "p33FJ06GS202.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP202
.include "p33FJ128GP202.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP204
.include "p33FJ128GP204.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP206
.include "p33FJ128GP206.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP306
.include "p33FJ128GP306.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP310
.include "p33FJ128GP310.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP706
.include "p33FJ128GP706.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP708
.include "p33FJ128GP708.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP710
.include "p33FJ128GP710.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP802
.include "p33FJ128GP802.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128GP804
.include "p33FJ128GP804.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128MC202
.include "p33FJ128MC202.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128MC204
.include "p33FJ128MC204.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128MC506
.include "p33FJ128MC506.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128MC510
.include "p33FJ128MC510.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128MC706
.include "p33FJ128MC706.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128MC708
.include "p33FJ128MC708.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128MC710
.include "p33FJ128MC710.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128MC802
.include "p33FJ128MC802.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ128MC804
.include "p33FJ128MC804.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ12GP201
.include "p33FJ12GP201.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ12GP202
.include "p33FJ12GP202.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ12MC201
.include "p33FJ12MC201.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ12MC202
.include "p33FJ12MC202.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ16GP304
.include "p33FJ16GP304.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ16GS402
.include "p33FJ16GS402.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ16GS404
.include "p33FJ16GS404.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ16GS502
.include "p33FJ16GS502.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ16GS504
.include "p33FJ16GS504.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ16MC304
.include "p33FJ16MC304.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ256GP506
.include "p33FJ256GP506.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ256GP510
.include "p33FJ256GP510.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ256GP710
.include "p33FJ256GP710.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ256MC510
.include "p33FJ256MC510.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ256MC710
.include "p33FJ256MC710.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ32GP202
.include "p33FJ32GP202.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ32GP204
.include "p33FJ32GP204.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ32GP302
.include "p33FJ32GP302.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ32GP304
.include "p33FJ32GP304.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ32MC202
.include "p33FJ32MC202.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ32MC204
.include "p33FJ32MC204.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ32MC302
.include "p33FJ32MC302.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ32MC304
.include "p33FJ32MC304.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP202
.include "p33FJ64GP202.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP204
.include "p33FJ64GP204.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP206
.include "p33FJ64GP206.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP306
.include "p33FJ64GP306.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP310
.include "p33FJ64GP310.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP706
.include "p33FJ64GP706.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP708
.include "p33FJ64GP708.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP710
.include "p33FJ64GP710.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP802
.include "p33FJ64GP802.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64GP804
.include "p33FJ64GP804.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64MC202
.include "p33FJ64MC202.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64MC204
.include "p33FJ64MC204.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64MC506
.include "p33FJ64MC506.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64MC508
.include "p33FJ64MC508.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64MC510
.include "p33FJ64MC510.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64MC706
.include "p33FJ64MC706.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64MC710
.include "p33FJ64MC710.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64MC802
.include "p33FJ64MC802.inc"
.equ VALID_ID,1
.endif

.ifdef __33FJ64MC804
.include "p33FJ64MC804.inc"
.equ VALID_ID,1
.endif

.ifndef VALID_ID
.error "processor ID not specified in generic include file"
.endif
