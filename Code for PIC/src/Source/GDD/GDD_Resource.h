/*****************************************************************************
 * FileName:        GDD_Resource.h
 * Processor:       PIC32MX
 * Compiler:        MPLAB C32/XC32 (see release notes for tested revision)
 * Linker:          MPLAB LINK32/XC32
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright(c) 2012 Microchip Technology Inc.  All rights reserved.
 * Microchip licenses to you the right to use, modify, copy and distribute
 * Software only when embedded on a Microchip microcontroller or digital
 * signal controller, which is integrated into your product or third party
 * product (pursuant to the sublicense terms in the accompanying license
 * agreement).
 *
 * You should refer to the license agreement accompanying this Software
 * for additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED ��AS IS�� WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
 * OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 * BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
 * COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
 * CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
 * OR OTHER SIMILAR COSTS.
 *
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * AUTO-GENERATED CODE:  Graphics Resource Converter version: 3.29.03
 *****************************************************************************/

#ifndef GDD_RESOURCE_H_FILE
#define GDD_RESOURCE_H_FILE
/*****************************************************************************
 * SECTION:  Includes
 *****************************************************************************/
#include <Graphics/Graphics.h>
#include "HardwareProfile.h"

/*****************************************************************************
 * SECTION:  Graphics Library Firmware Check
 *****************************************************************************/
#if(GRAPHICS_LIBRARY_VERSION != 0x0306)
#warning "It is suggested to use Graphics Library version 3.06 with this version of GRC."
#endif

/*****************************************************************************
 * SECTION:  Graphics Bitmap Padding Check
 *****************************************************************************/
#ifdef USE_BITMAP_NO_PADDING_LINE
#error These bitmap resources have been converted with padding of the horizontal lines, but USE_BITMAP_NO_PADDING_LINE is defined in GraphicsConfig.h.
#endif


/*****************************************************************************
 * This is an error check for the color depth
 *****************************************************************************/
#if (COLOR_DEPTH > 16)
#error "Color Depth needs to be 16 to correctly use these resources"
#endif



/*****************************************************************************
 * SECTION:  BITMAPS
 *****************************************************************************/

/*********************************
 * Bitmap Structure
 * Label: BLUE_NEXT4040
 * Description:  40x40 pixels, 16-bits per pixel
 ***********************************/
extern const IMAGE_FLASH BLUE_NEXT4040;
#define BLUE_NEXT4040_WIDTH     (40)
#define BLUE_NEXT4040_HEIGHT    (40)
#define BLUE_NEXT4040_SIZE      (3206)
/*********************************
 * Bitmap Structure
 * Label: BLUE_PREVIOUS4040
 * Description:  40x40 pixels, 16-bits per pixel
 ***********************************/
extern const IMAGE_FLASH BLUE_PREVIOUS4040;
#define BLUE_PREVIOUS4040_WIDTH     (40)
#define BLUE_PREVIOUS4040_HEIGHT    (40)
#define BLUE_PREVIOUS4040_SIZE      (3206)
/*********************************
 * Bitmap Structure
 * Label: img_default
 * Description:  72x72 pixels, 4-bits per pixel
 ***********************************/
extern const IMAGE_FLASH img_default;
#define img_default_WIDTH     (72)
#define img_default_HEIGHT    (72)
#define img_default_SIZE      (2630)
/*****************************************************************************
 * SECTION:  Fonts
 *****************************************************************************/

/*********************************
 * Installed Font Structure
 * Label: Arial_20
 * Description:  Height: 24 pixels, 1 bit per pixel, Range: ' ' to '~'
 ***********************************/
extern const FONT_FLASH Arial_20;
#define Arial_20_SIZE    (4396)
/*********************************
 * Installed Font Structure
 * Label: Aharoni_Bold_24
 * Description:  Height: 25 pixels, 1 bit per pixel, Range: ' ' to ''
 ***********************************/
extern const FONT_FLASH Aharoni_Bold_24;
#define Aharoni_Bold_24_SIZE    (5042)
/*********************************
 * Installed Font Structure
 * Label: Monospaced_plain_38
 * Description:  Height: 56 pixels, 1 bit per pixel, Range: ' ' to ''
 ***********************************/
extern const FONT_FLASH Monospaced_plain_38;
#define Monospaced_plain_38_SIZE    (16520)
/*********************************
 * TTF Font File Structure
 * Label: Gentium_16
 * Description:  Height: 19 pixels, 1 bit per pixel, Range: ' ' to '~'
 ***********************************/
extern const FONT_FLASH Gentium_16;
#define Gentium_16_SIZE    (2668)
#endif

