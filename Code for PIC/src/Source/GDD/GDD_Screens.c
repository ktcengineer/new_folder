
/*****************************************************************************
* Microchip Graphics Library
* Graphics Display Designer (GDD) Template
*****************************************************************************
 
* Dependencies:    See INCLUDES section below
* Processor:       PIC24F, PIC24H, dsPIC, PIC32
* Compiler:        MPLAB C30 V3.22, MPLAB C32 V1.05b
* Linker:          MPLAB LINK30, MPLAB LINK32
* Company:         Microchip Technology Incorporated
*
* Software License Agreement
*
* Copyright (c) 2010 Microchip Technology Inc.  All rights reserved.
* Microchip licenses to you the right to use, modify, copy and distribute
* Software only when embedded on a Microchip microcontroller or digital
* signal controller, which is integrated into your product or third party
* product (pursuant to the sublicense terms in the accompanying license
* agreement).  
*
* You should refer to the license agreement accompanying this Software
* for additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
* OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
* PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
* OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 
* BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
* DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
* INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
* COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
* CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
* OR OTHER SIMILAR COSTS.
*
* Author               Date        Comment
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*****************************************************************************/


/***************************************************
*INCLUDE FILES
***************************************************/
#include "Graphics/Graphics.h"
#include "GDD_Screens.h"
#include"chn.h"
#include"wk160.h"
#include"CW.h"
#include"SHZ.h"
#include"LGF.h"
/***************************************************
* String literals used in the project
***************************************************/
const XCHAR src1_STE_18text[ ] = "static text";

const XCHAR src1_OTE_3text[ ] = "WeiKeng-Qingdao";

const XCHAR src2_BTN_23text[ ] = "Button";
const XCHAR src2_BTN_25text[ ] = "Button";
const XCHAR src2_STE_28text[ ] = "2013-10-26 15:14:23";
const XCHAR src2_BTN_100text[ ] = "";
const XCHAR src2_BTN_101text[ ] = "";

const XCHAR src2_OTE_6text[ ] = "Main Meun";

const XCHAR src3_BTN_30text[ ] = "Start";
const XCHAR src3_BTN_31text[ ] = "Stop";
const XCHAR src3_BTN_41text[ ] = "Back";
const XCHAR src3_BTN_42text[ ] = "Next";

const XCHAR src4_STE_32text[ ] = "2013-10-26 15:14:23";
const XCHAR src4_BTN_36text[ ] = "Start";
const XCHAR src4_BTN_37text[ ] = "Stop";
const XCHAR src4_BTN_38text[ ] = "Pause";
const XCHAR src4_BTN_43text[ ] = "Back";
const XCHAR src4_BTN_44text[ ] = "Next";

const XCHAR src5_STE_45text[ ] = "2013-10-26 15:14:23";
const XCHAR src5_BTN_48text[ ] = "Button";
const XCHAR src5_STE_61text[ ] = "Static";
const XCHAR src5_BTN_62text[ ] = "Back";
const XCHAR src5_BTN_63text[ ] = "Next";
const XCHAR src5_STE_64text[ ] = "Static";
const XCHAR src5_STE_65text[ ] = "Static";
const XCHAR src5_STE_66text[ ] = "Static";
const XCHAR src5_STE_67text[ ] = "Static";
const XCHAR src5_STE_68text[ ] = "Static";

const XCHAR src5_OTE_28text[ ] = "YY:";
const XCHAR src5_OTE_30text[ ] = "MM:";
const XCHAR src5_OTE_32text[ ] = "DD:";
const XCHAR src5_OTE_34text[ ] = ":HH";
const XCHAR src5_OTE_36text[ ] = ":MM";
const XCHAR src5_OTE_38text[ ] = ":SS";

const XCHAR src6_STE_72text[ ] = "-2         -45         60         32C";
const XCHAR src6_STE_76text[ ] = "Static";
const XCHAR src6_STE_77text[ ] = "Static";
const XCHAR src6_STE_78text[ ] = "Static";
const XCHAR src6_BTN_87text[ ] = "Graphic";
const XCHAR src6_BTN_90text[ ] = "Read";
const XCHAR src6_BTN_91text[ ] = "Write";
const XCHAR src6_BTN_92text[ ] = "Back";
const XCHAR src6_BTN_93text[ ] = "Next";

const XCHAR src6_OTE_56text[ ] = "X";
const XCHAR src6_OTE_58text[ ] = "Y";
const XCHAR src6_OTE_60text[ ] = "Z";
const XCHAR src6_OTE_62text[ ] = "Temp";

const XCHAR src7_BTN_94text[ ] = "Download";
const XCHAR src7_BTN_97text[ ] = "Back";

const XCHAR src8_STE_95text[ ] = "No Image,Finding";
const XCHAR src8_BTN_98text[ ] = "Back";



/***************************************************
* Scheme Declarations
***************************************************/
GOL_SCHEME* defscheme;
GOL_SCHEME* enter_new_scheme1;
GOL_SCHEME* enter_new_scheme;
GOL_SCHEME* enter_new_scheme3;
GOL_SCHEME* enter_new_scheme_src7;


/***************************************************
* Function and global Declarations
***************************************************/
void (*CreateFunctionArray[NUM_GDD_SCREENS])();
void (*CreatePrimitivesFunctionArray[NUM_GDD_SCREENS])();
WORD currentGDDDemoScreenIndex;
BYTE update = 0;
static BYTE updateGPL = 0;

/***************************************************
* Function       : GDDDemoCreateFirstScreen
* Parameters     : none
* Return         : none
* Description    : Creates the first screen
***************************************************/
void GDDDemoCreateFirstScreen(void)
{
    currentGDDDemoScreenIndex = 0;
    update = 1;
    (*CreateFunctionArray[currentGDDDemoScreenIndex])();
}

/***************************************************
* Function      : GDDDemoNextScreen
* Parameters    : none
* Return        : none
* Description   : Updates counter to show next screen
***************************************************/
void GDDDemoNextScreen(void)
{
    currentGDDDemoScreenIndex++;
    if(currentGDDDemoScreenIndex >= NUM_GDD_SCREENS)
    {
        currentGDDDemoScreenIndex = 0;
    }
    update = 1;
}

/***************************************************
* Function      : GDDDemoGoToScreen
* Parameters    : int screenIndex
* Return        : none
* Description   : Show the screen referred by the index
***************************************************/
void GDDDemoGoToScreen(int screenIndex)
{
    currentGDDDemoScreenIndex = screenIndex;
    if(currentGDDDemoScreenIndex >= NUM_GDD_SCREENS)
    {
        currentGDDDemoScreenIndex = 0;
    }
    update = 1;
}

/***************************************************
* Function       : GDDDemoGOLDrawCallback
* Parameters     : none
* Return         : none
* Description    : Callback to do the actual drawing of widgets
***************************************************/
void GDDDemoGOLDrawCallback(void)
{
    if(updateGPL)
    {
        (*CreatePrimitivesFunctionArray[currentGDDDemoScreenIndex])();
        updateGPL = 0;
    }

    if(update)
    {
        (*CreateFunctionArray[currentGDDDemoScreenIndex])();
        if(CreatePrimitivesFunctionArray[currentGDDDemoScreenIndex] != NULL)
        {
            updateGPL = 1;
        }
        update = 0;
    }
}

/***************************************************
* Function       : CreateError
* Parameters     : none
* Return         : none
* Description    : Creates a Error screen 
***************************************************/
void CreateError(char* string)
{
    // Blue Screen Error
    SetColor(119);
    ClearDevice();
    SetColor(-1);

    // Flash Error Message
    if(string == NULL)
        {OutTextXY(0, 0, "Runtime Error.");}
    else
        {OutTextXY(0,0, string);}
}

/***************************************************
* Function 	      :    Createsrc1
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - src1
***************************************************/
void Createsrc1(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Gentium_16;

    if(enter_new_scheme3 != NULL) free(enter_new_scheme3);
        enter_new_scheme3 = GOLCreateScheme();

    enter_new_scheme3->Color0 = RGBConvert(32, 168, 224);
    enter_new_scheme3->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme3->TextColor0 = RGBConvert(24, 24, 24);
    enter_new_scheme3->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme3->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme3->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme3->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme3->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme3->CommonBkColor = RGBConvert(248, 252, 248);
    enter_new_scheme3->pFont = (void*)&_____42;


    PICTURE *pPCB_15;
    pPCB_15 = PictCreate(  PCB_15, //name
                       0, //left
                       0, //top
                       319, //right
                       160, //bottom
                       PICT_DRAW, //state
                       1, //scale
                       (void*)&weikeng160, //bitmap
                      defscheme //scheme
                    );

    if(pPCB_15==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_18;
    pSTE_18 = StCreate(  STE_18, //name
                       0, //left
                       161, //top
                       319, //right
                       220, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)garin, //text
                      enter_new_scheme3 //scheme
                    );

    if(pSTE_18==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createsrc2
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - src2
***************************************************/
void Createsrc2(void)
{
    
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&___12;

    if(enter_new_scheme1 != NULL) free(enter_new_scheme1);
        enter_new_scheme1 = GOLCreateScheme();

    enter_new_scheme1->Color0 = RGBConvert(32, 168, 224);
    enter_new_scheme1->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme1->TextColor0 = RGBConvert(248, 48, 48);
    enter_new_scheme1->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme1->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme1->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme1->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme1->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme1->CommonBkColor = RGBConvert(208, 236, 240);
    enter_new_scheme1->pFont = (void*)&Arial_20;


    BUTTON *pBTN_23;
    pBTN_23 = BtnCreate(  BTN_23, //name
                       112, //left
                       50, //top
                       222, //right
                       96, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)app, //text
                      defscheme //scheme
                    );

    if(pBTN_23==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_25;
    pBTN_25 = BtnCreate(  BTN_25, //name
                       112, //left
                       107, //top
                       222, //right
                       151, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)download, //text
                      defscheme //scheme
                    );

    if(pBTN_25==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

 //src2_STE_28text[ ] = "Static Text";
    STATICTEXT *pSTE_28;
    pSTE_28 = StCreate(  STE_28, //name
                       0, //left
                       159, //top
                       319, //right
                       191, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)src2_STE_28text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pSTE_28==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_100;
    pBTN_100 = BtnCreate(  BTN_100, //name
                       0, //left
                       199, //top
                       40, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       (void*)&BLUE_PREVIOUS4040, //bitmap
                       (XCHAR*)src2_BTN_100text, //text
                      defscheme //scheme
                    );

    if(pBTN_100==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_101;
    pBTN_101 = BtnCreate(  BTN_101, //name
                       279, //left
                       199, //top
                       319, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       (void*)&BLUE_NEXT4040, //bitmap
                       (XCHAR*)src2_BTN_101text, //text
                      defscheme //scheme
                    );

    if(pBTN_101==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }
/*
XCHAR text[ ]={"Peter.Gao"};
    STATICTEXT *src2st;
src2st=GOLFindObject(STE_28);
src2st->pText=text;
SetState(src2st,ST_UPDATE);
*/
}
/***************************************************
* Function 	      :    Createsrc3
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - src3
***************************************************/
void Createsrc3(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Gentium_16;

    if(enter_new_scheme1 != NULL) free(enter_new_scheme1);
        enter_new_scheme1 = GOLCreateScheme();

    enter_new_scheme1->Color0 = RGBConvert(32, 168, 224);
    enter_new_scheme1->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme1->TextColor0 = RGBConvert(248, 48, 48);
    enter_new_scheme1->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme1->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme1->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme1->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme1->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme1->CommonBkColor = RGBConvert(208, 236, 240);
    enter_new_scheme1->pFont = (void*)&Arial_20;


    SLIDER *pSLD_29;
    pSLD_29 = SldCreate(  SLD_29, //name
                       71, //left
                       129, //top
                       245, //right
                       159, //bottom
                       SLD_DRAW | SLD_DRAW_THUMB, //state
                       100, //range
                       1, //pagesize
                       1, //pos
                      defscheme //scheme
                    );

    if(pSLD_29==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_30;
    pBTN_30 = BtnCreate(  BTN_30, //name
                       23, //left
                       38, //top
                       133, //right
                       78, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src3_BTN_30text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_30==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_31;
    pBTN_31 = BtnCreate(  BTN_31, //name
                       183, //left
                       38, //top
                       293, //right
                       78, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src3_BTN_31text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_31==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_41;
    pBTN_41 = BtnCreate(  BTN_41, //name
                       0, //left
                       199, //top
                       73, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src3_BTN_41text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_41==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_42;
    pBTN_42 = BtnCreate(  BTN_42, //name
                       245, //left
                       199, //top
                       316, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src3_BTN_42text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_42==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createsrc4
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - src4
***************************************************/
void Createsrc4(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(enter_new_scheme1 != NULL) free(enter_new_scheme1);
        enter_new_scheme1 = GOLCreateScheme();

    enter_new_scheme1->Color0 = RGBConvert(32, 168, 224);
    enter_new_scheme1->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme1->TextColor0 = RGBConvert(248, 48, 48);
    enter_new_scheme1->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme1->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme1->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme1->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme1->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme1->CommonBkColor = RGBConvert(208, 236, 240);
    enter_new_scheme1->pFont = (void*)&Arial_20;


    STATICTEXT *pSTE_32;
    pSTE_32 = StCreate(  STE_32, //name
                       0, //left
                       51, //top
                       319, //right
                       87, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)src4_STE_32text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pSTE_32==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_36;
    pBTN_36 = BtnCreate(  BTN_36, //name
                       0, //left
                       142, //top
                       75, //right
                       180, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src4_BTN_36text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_36==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_37;
    pBTN_37 = BtnCreate(  BTN_37, //name
                       244, //left
                       142, //top
                       319, //right
                       180, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src4_BTN_37text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_37==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_38;
    pBTN_38 = BtnCreate(  BTN_38, //name
                       126, //left
                       142, //top
                       201, //right
                       180, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src4_BTN_38text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_38==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_43;
    pBTN_43 = BtnCreate(  BTN_43, //name
                       0, //left
                       199, //top
                       73, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src4_BTN_43text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_43==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_44;
    pBTN_44 = BtnCreate(  BTN_44, //name
                       248, //left
                       199, //top
                       319, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src4_BTN_44text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_44==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createsrc5
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - src5
***************************************************/
void Createsrc5(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(enter_new_scheme1 != NULL) free(enter_new_scheme1);
        enter_new_scheme1 = GOLCreateScheme();

    enter_new_scheme1->Color0 = RGBConvert(32, 168, 224);
    enter_new_scheme1->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme1->TextColor0 = RGBConvert(248, 48, 48);
    enter_new_scheme1->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme1->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme1->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme1->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme1->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme1->CommonBkColor = RGBConvert(208, 236, 240);
    enter_new_scheme1->pFont = (void*)&Arial_20;

    if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Gentium_16;

    if(enter_new_scheme != NULL) free(enter_new_scheme);
        enter_new_scheme = GOLCreateScheme();

    enter_new_scheme->Color0 = RGBConvert(0, 0, 0);
    enter_new_scheme->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme->TextColor0 = RGBConvert(24, 24, 24);
    enter_new_scheme->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme->CommonBkColor = RGBConvert(248, 252, 248);
    enter_new_scheme->pFont = (void*)&Gentium_16;


    STATICTEXT *pSTE_45;
    pSTE_45 = StCreate(  STE_45, //name
                       0, //left
                       0, //top
                       319, //right
                       36, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)src5_STE_45text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pSTE_45==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    ROUNDDIAL *pRDI_46;
    pRDI_46 = RdiaCreate(  RDI_46, //name
                       162, //xcenter
                       92, //ycenter
                       56, //radius
                       RDIA_DRAW, //state
                       2, //res
                       50, //value
                       100, //max
                      defscheme //scheme
                    );

    if(pRDI_46==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    SLIDER *pSLD_47;
    pSLD_47 = SldCreate(  SLD_47, //name
                       81, //left
                       209, //top
                       242, //right
                       239, //bottom
                       SLD_DRAW | SLD_DRAW_THUMB, //state
                       100, //range
                       1, //pagesize
                       10, //pos
                      defscheme //scheme
                    );

    if(pSLD_47==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }
#if 0
    BUTTON *pBTN_48;
    pBTN_48 = BtnCreate(  BTN_48, //name
                       108, //left
                       156, //top
                       218, //right
                       196, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src5_BTN_48text, //text
                      defscheme //scheme
                    );

    if(pBTN_48==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }
#endif
    STATICTEXT *pSTE_61;
    pSTE_61 = StCreate(  STE_61, //name
                       27, //left
                       46, //top
                       82, //right
                       76, //bottom
                       ST_DRAW, //state
                       (XCHAR*)src5_STE_61text, //text
                      enter_new_scheme //scheme
                    );

    if(pSTE_61==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_62;
    pBTN_62 = BtnCreate(  BTN_62, //name
                       0, //left
                       199, //top
                       73, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src5_BTN_62text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_62==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_63;
    pBTN_63 = BtnCreate(  BTN_63, //name
                       248, //left
                       199, //top
                       319, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src5_BTN_63text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_63==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_64;
    pSTE_64 = StCreate(  STE_64, //name
                       30, //left
                       106, //top
                       85, //right
                       136, //bottom
                       ST_DRAW, //state
                       (XCHAR*)src5_STE_64text, //text
                      enter_new_scheme //scheme
                    );

    if(pSTE_64==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_65;
    pSTE_65 = StCreate(  STE_65, //name
                       26, //left
                       165, //top
                       81, //right
                       195, //bottom
                       ST_DRAW, //state
                       (XCHAR*)src5_STE_65text, //text
                      enter_new_scheme //scheme
                    );

    if(pSTE_65==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_66;
    pSTE_66 = StCreate(  STE_66, //name
                       234, //left
                       47, //top
                       289, //right
                       77, //bottom
                       ST_DRAW | ST_RIGHT_ALIGN, //state
                       (XCHAR*)src5_STE_66text, //text
                      enter_new_scheme //scheme
                    );

    if(pSTE_66==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_67;
    pSTE_67 = StCreate(  STE_67, //name
                       234, //left
                       106, //top
                       289, //right
                       136, //bottom
                       ST_DRAW | ST_RIGHT_ALIGN, //state
                       (XCHAR*)src5_STE_67text, //text
                      enter_new_scheme //scheme
                    );

    if(pSTE_67==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_68;
    pSTE_68 = StCreate(  STE_68, //name
                       234, //left
                       165, //top
                       289, //right
                       195, //bottom
                       ST_DRAW | ST_RIGHT_ALIGN, //state
                       (XCHAR*)src5_STE_68text, //text
                      enter_new_scheme //scheme
                    );

    if(pSTE_68==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createsrc6
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - src6
***************************************************/
void Createsrc6(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Arial_20;

    if(enter_new_scheme1 != NULL) free(enter_new_scheme1);
        enter_new_scheme1 = GOLCreateScheme();

    enter_new_scheme1->Color0 = RGBConvert(32, 168, 224);
    enter_new_scheme1->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme1->TextColor0 = RGBConvert(248, 48, 48);
    enter_new_scheme1->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme1->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme1->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme1->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme1->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme1->CommonBkColor = RGBConvert(208, 236, 240);
    enter_new_scheme1->pFont = (void*)&Arial_20;


    STATICTEXT *pSTE_72;
    pSTE_72 = StCreate(  STE_72, //name
                       9, //left
                       67, //top
                       314, //right
                       100, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)src6_STE_72text, //text
                      defscheme //scheme
                    );

    if(pSTE_72==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    
    BUTTON *pBTN_87;
    pBTN_87 = BtnCreate(  BTN_87, //name
                       9, //left
                       143, //top
                       92, //right
                       183, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src6_BTN_87text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_87==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_90;
    pBTN_90 = BtnCreate(  BTN_90, //name
                       229, //left
                       143, //top
                       312, //right
                       183, //bottom
                       0, //radius
                      BTN_HIDE, //state
                       NULL, //bitmap
                       (XCHAR*)src6_BTN_90text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_90==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_91;
    pBTN_91 = BtnCreate(  BTN_91, //name
                       119, //left
                       143, //top
                       202, //right
                       183, //bottom
                       0, //radius
                       BTN_HIDE, //state
                       NULL, //bitmap
                       (XCHAR*)src6_BTN_91text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_91==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_92;
    pBTN_92 = BtnCreate(  BTN_92, //name
                       0, //left
                       199, //top
                       73, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src6_BTN_92text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_92==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_93;
    pBTN_93 = BtnCreate(  BTN_93, //name
                       248, //left
                       199, //top
                       319, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src6_BTN_93text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_93==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createsrc7
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - src7
***************************************************/
void Createsrc7(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(enter_new_scheme_src7 != NULL) free(enter_new_scheme_src7);
        enter_new_scheme_src7 = GOLCreateScheme();

    enter_new_scheme_src7->Color0 = RGBConvert(32, 168, 224);
    enter_new_scheme_src7->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme_src7->TextColor0 = RGBConvert(24, 24, 24);
    enter_new_scheme_src7->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme_src7->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme_src7->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme_src7->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme_src7->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme_src7->CommonBkColor = RGBConvert(208, 236, 240);
    enter_new_scheme_src7->pFont = (void*)&Monospaced_plain_38;

    if(enter_new_scheme1 != NULL) free(enter_new_scheme1);
        enter_new_scheme1 = GOLCreateScheme();

    enter_new_scheme1->Color0 = RGBConvert(32, 168, 224);
    enter_new_scheme1->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme1->TextColor0 = RGBConvert(248, 48, 48);
    enter_new_scheme1->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme1->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme1->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme1->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme1->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme1->CommonBkColor = RGBConvert(208, 236, 240);
    enter_new_scheme1->pFont = (void*)&Arial_20;


    BUTTON *pBTN_94;
    pBTN_94 = BtnCreate(  BTN_94, //name
                       76, //left
                       70, //top
                       249, //right
                       153, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src7_BTN_94text, //text
                      enter_new_scheme_src7 //scheme
                    );

    if(pBTN_94==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_97;
    pBTN_97 = BtnCreate(  BTN_97, //name
                       0, //left
                       199, //top
                       73, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src7_BTN_97text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_97==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createsrc8
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - src8
***************************************************/
void Createsrc8(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(enter_new_scheme1 != NULL) free(enter_new_scheme1);
        enter_new_scheme1 = GOLCreateScheme();

    enter_new_scheme1->Color0 = RGBConvert(32, 168, 224);
    enter_new_scheme1->Color1 = RGBConvert(16, 132, 168);
    enter_new_scheme1->TextColor0 = RGBConvert(248, 48, 48);
    enter_new_scheme1->TextColor1 = RGBConvert(248, 252, 248);
    enter_new_scheme1->EmbossDkColor = RGBConvert(248, 204, 0);
    enter_new_scheme1->EmbossLtColor = RGBConvert(24, 116, 184);
    enter_new_scheme1->TextColorDisabled = RGBConvert(128, 128, 128);
    enter_new_scheme1->ColorDisabled = RGBConvert(208, 224, 240);
    enter_new_scheme1->CommonBkColor = RGBConvert(208, 236, 240);
    enter_new_scheme1->pFont = (void*)&Arial_20;

    if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Gentium_16;


    STATICTEXT *pSTE_95;
    pSTE_95 = StCreate(  STE_95, //name
                       30, //left
                       45, //top
                       299, //right
                       86, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)src8_STE_95text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pSTE_95==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_98;
    pBTN_98 = BtnCreate(  BTN_98, //name
                       0, //left
                       199, //top
                       73, //right
                       239, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)src8_BTN_98text, //text
                      enter_new_scheme1 //scheme
                    );

    if(pBTN_98==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    PROGRESSBAR *pPRB_73;
    pPRB_73 = PbCreate(  PRB_73, //name
                       29, //left
                       111, //top
                       299, //right
                       141, //bottom
                       PB_DRAW | PB_DRAW_BAR, //state
                       0, //pos
                       100, //range
                      defscheme //scheme
                    );

    if(pPRB_73==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    CreateGraphic
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - Graphic
***************************************************/
void CreateGraphic(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


}
/***************************************************
* Function 	      :    Createsrc1
* Parameters      :    none
* Return          :    none
* Description     :    Creates GPL widgets used in screen - src1
***************************************************/
void CreatePrimitivesForsrc1(void)
{
    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Gentium_16);
    while(!OutTextXY(  98, //x
                       220, //x
                      (XCHAR*)src1_OTE_3text //text
                    )
         );


}



/***************************************************
* Function 	      :    Createsrc2
* Parameters      :    none
* Return          :    none
* Description     :    Creates GPL widgets used in screen - src2
***************************************************/
void CreatePrimitivesForsrc2(void)
{
    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Aharoni_Bold_24);
    while(!OutTextXY(  112, //x
                       15, //x
                      (XCHAR*)src2_OTE_6text //text
                    )
         );


}



/***************************************************
* Function 	      :    Createsrc5
* Parameters      :    none
* Return          :    none
* Description     :    Creates GPL widgets used in screen - src5
***************************************************/
void CreatePrimitivesForsrc5(void)
{
    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Gentium_16);
    while(!OutTextXY(  0, //x
                       47, //x
                      (XCHAR*)src5_OTE_28text //text
                    )
         );

    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Gentium_16);
    while(!OutTextXY(  0, //x
                       106, //x
                      (XCHAR*)src5_OTE_30text //text
                    )
         );

    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Gentium_16);
    while(!OutTextXY(  0, //x
                       165, //x
                      (XCHAR*)src5_OTE_32text //text
                    )
         );

    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Gentium_16);
    while(!OutTextXY(  290, //x
                       47, //x
                      (XCHAR*)src5_OTE_34text //text
                    )
         );

    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Gentium_16);
    while(!OutTextXY(  289, //x
                       106, //x
                      (XCHAR*)src5_OTE_36text //text
                    )
         );

    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Gentium_16);
    while(!OutTextXY(  290, //x
                       165, //x
                      (XCHAR*)src5_OTE_38text //text
                    )
         );


}



/***************************************************
* Function 	      :    Createsrc6
* Parameters      :    none
* Return          :    none
* Description     :    Creates GPL widgets used in screen - src6
***************************************************/
void CreatePrimitivesForsrc6(void)
{
    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Monospaced_plain_38);
    while(!OutTextXY(  32, //x
                       11, //x
                      (XCHAR*)src6_OTE_56text //text
                    )
         );

    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Monospaced_plain_38);
    while(!OutTextXY(  112, //x
                       11, //x
                      (XCHAR*)src6_OTE_58text //text
                    )
         );

    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Monospaced_plain_38);
    while(!OutTextXY(  193, //x
                       12, //x
                      (XCHAR*)src6_OTE_60text //text
                    )
         );

    SetLineType(0);
    SetLineThickness(0);
    SetColor(RGBConvert(0, 0, 0));
    SetFont((void*)&Aharoni_Bold_24);
    while(!OutTextXY(  251, //x
                       32, //x
                      (XCHAR*)src6_OTE_62text //text
                    )
         );


}




/***************************************************
* Function       : CreateFunctionArray
* Parameters     : none
* Return         : none
* Description    : Creates a array of GOL function pointers
***************************************************/
void (*CreateFunctionArray[NUM_GDD_SCREENS])(void)=
    
{
    &Createsrc1,
    &Createsrc2,
    &Createsrc3,
    &Createsrc4,
    &Createsrc5,
    &Createsrc6,
    &Createsrc7,
    &Createsrc8,
    &CreateGraphic,
};



/***************************************************
* Function       : CreatePrimitivesFunctionArray
* Parameters     : none
* Return         : none
* Description    : Creates a array of GPL function pointers
***************************************************/
void (*CreatePrimitivesFunctionArray[NUM_GDD_SCREENS])(void)=
    
{
    &CreatePrimitivesForsrc1,
    &CreatePrimitivesForsrc2,
    NULL,
    NULL,
    &CreatePrimitivesForsrc5,
    &CreatePrimitivesForsrc6,
    NULL,
    NULL,
    NULL,
};


