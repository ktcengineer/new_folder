/* 
 * File:   soft_rtcc.c
 * Author: Administrator
 *
 * Created on 2013年7月26日, 上午6:01
 */

#include <stdio.h>
#include <stdlib.h>
#include <plib.h>
#include "soft_rtcc.h"



// Number of days in a leap year
#define LEAP_DAY 29

// Number of days in each month
enum
{ January , February , March , April , May , June , July , August , September ,October , November , December };

// The month table used in day of week calculation
// January 6 for leap year, February 2 for leap year
const unsigned char monthsTable [12] = {0,3,3,6,1,4,6,2,5,0,3,5};

// The number of days in each month
const unsigned char DaysInMonth [12]={31,28,31,30,31,30,31,31,30,31,30,31};

void soft_rtcc_init(void)
{
    rtcc.time.seconds = 15;
    rtcc.time.minutes = 27;
    rtcc.time.hour = 14;
    rtcc.date.day = 5;
    rtcc.date.dayofweek = 26;
    rtcc.date.month = 7;
    rtcc.date.year = 13;
}

// this function receives the current day as parameter
// and returns the day of week
unsigned char DayOfWeek ( DATETIME getDayOfWeek )
{
	int temp = 0;
	temp += 6;
	temp += getDayOfWeek.date.year;
	temp += (int) (getDayOfWeek.date.year>>2);
	temp += monthsTable[getDayOfWeek.date.month-1];
	temp += getDayOfWeek.date.day;
	// leap year correction
	if (!(getDayOfWeek.date.year%4))
	{
		if (getDayOfWeek.date.month == January )
			temp += 6;
		if (getDayOfWeek.date.month == February )
			temp -= 1;
	}
	return (temp%7);

}

void __ISR(_TIMER_5_VECTOR, ipl3) _T5Interrupt(void)
{
// service the clock
rtcc.time.seconds++;
if ( rtcc.time.seconds >= 60 )
{
    rtcc.time.seconds = 0;
    rtcc.time.minutes++;
    if ( rtcc.time.minutes >= 60 )
    {
        rtcc.time.minutes = 0;
        rtcc.time.hour++;
        if ( rtcc.time.hour >= 24 )
        {
            rtcc.time.hour = 0;

            // update calendar
            rtcc.date.day++;
            if ( rtcc.date.day > DaysInMonth [rtcc.date.month]  )
            {
                // see if this is a leap year
                if ( rtcc.date.month != February )
                {
                    rtcc.date.month++;
                    rtcc.date.day = 1;
                }
                if ( (!(rtcc.date.year%4)) && (rtcc.date.month == February) )  // leap years are multiple of 4
                {
                    if ( rtcc.date.day > 29)
                    {
                        rtcc.date.month++;
                        rtcc.date.day = 1;
                    }
                }

                if  ( rtcc.date.month > 12 )
                {
                    rtcc.date.year++;
                    rtcc.date.month=1;
                    rtcc.date.day =1;
                }
            }

            // end update calendar

            // get day of week
            // no need to to day of week here
            rtcc.date.dayofweek = DayOfWeek ( rtcc );
        }
    }
    
}

    // Simple alarm demo, uses an LED as alarm
    if ( rtcc.date.year 	== alarm.date.year 	&&
    rtcc.date.month 	== alarm.date.month &&
    rtcc.date.day 		== alarm.date.day 	&&
    rtcc.time.hour 	== alarm.time.hour 	&&
    rtcc.time.minutes	== alarm.time.minutes )
    {
    // trigger the alarm

    }
    else
    {

    }
    mT5ClearIntFlag();
}

void rtcc_to_string(void)
{
    sprintf((char *)rtcc_str, "%d-%d-%d %d:%d:%d", rtcc.date.year,rtcc.date.month,rtcc.date.day,rtcc.time.hour,rtcc.time.minutes,rtcc.time.seconds);
}
