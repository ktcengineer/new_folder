/********************************************************************
 FileName:     usb_host_bootloader.c
 Dependencies: See INCLUDES section
 Processor:		PIC32 USB MSD Microcontrollers
 Hardware:		
 Complier:  	Microchip C18 (for PIC18), C30 (for PIC24), C32 (for PIC32)
 Company:		Microchip Technology, Inc.

 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the �Company�) for its PIC� Microcontroller is intended and
 supplied to you, the Company�s customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.

********************************************************************
 File Description:

 Change History:
  Rev   Description
  1.0   Initial release
  2.1   Updated for simplicity and to use common
                     coding style
********************************************************************/
#include "MDD File System/FSIO.h"
#include "NVMem.h"
#include"Main_L.h"
#include "Bootloader.h"
#include <plib.h>
#include "HardwareProfile.h"
#include "GDD_Screens.h"//Graphics.
#include "Accelerometer.h"//Accelerate
#include "soft_rtcc.h"
//Graphics defined start
/////////////////////////////////////////////////////////////////////////////
// SPI Device Initialization Function
/////////////////////////////////////////////////////////////////////////////
#if defined (USE_SST25VF016)
    // initialize GFX3 SST25 flash SPI
    #define FlashInit(pInitData) SST25Init((DRV_SPI_INIT_DATA*)pInitData)
#elif defined (USE_MCHP25LC256)
    // initialize EEPROM on Explorer 16
    #define FlashInit(pInitData) MCHP25LC256Init((DRV_SPI_INIT_DATA*)pInitData)
#elif defined (USE_M25P80)
    #define FlashInit(pInitData) SST25Init((DRV_SPI_INIT_DATA*)pInitData)
#endif
/////////////////////////////////////////////////////////////////////////////
// SPI Channel settings
/////////////////////////////////////////////////////////////////////////////
#if defined (SPI_CHANNEL_1_ENABLE) || defined (SPI_CHANNEL_2_ENABLE) || defined (SPI_CHANNEL_3_ENABLE) || defined (SPI_CHANNEL_4_ENABLE)
    #if defined (USE_SST25VF016)
        #ifdef __PIC32MX
            const DRV_SPI_INIT_DATA SPI_Init_Data = {SST25_SPI_CHANNEL, 1, 0, 0, 1, 1, 0};
            #ifdef USE_TOUCHSCREEN_AR1020
                const DRV_SPI_INIT_DATA ar1020SpiInit = {AR1020_SPI_CHANNEL,    44, 0, 0, 0, 0, 0};
            #endif
        #else
            const DRV_SPI_INIT_DATA SPI_Init_Data = {SST25_SPI_CHANNEL, 3, 6, 0, 1, 1, 0};
            #ifdef USE_TOUCHSCREEN_AR1020
                const DRV_SPI_INIT_DATA ar1020SpiInit = {AR1020_SPI_CHANNEL,    2,  3, 0, 0, 0, 0};
            #endif
        #endif
    #elif defined (USE_MCHP25LC256)
        const DRV_SPI_INIT_DATA SPI_Init_Data = {MCHP25LC256_SPI_CHANNEL, 6, 3, 0, 1, 1, 0};
    #elif defined (USE_M25P80)
            const DRV_SPI_INIT_DATA SPI_Init_Data = {SST25_SPI_CHANNEL, 3, 6, 0, 1, 1, 0};
    #endif
#endif

/////////////////////////////////////////////////////////////////////////////
// TouchScreen Init Values
/////////////////////////////////////////////////////////////////////////////
#ifdef USE_TOUCHSCREEN_RESISTIVE
#define TOUCH_INIT_VALUES   (NULL)
#endif
#ifdef USE_TOUCHSCREEN_AR1020
#define TOUCH_INIT_VALUES   ((void *)&ar1020SpiInit)
#endif

//Graphics defined end
// *****************************************************************************
// *****************************************************************************
// Device Configuration Bits (Runs from Aux Flash)
// *****************************************************************************
// *****************************************************************************
// Configuring the Device Configuration Registers
// 80Mhz Core/Periph, Pri Osc w/PLL, Write protect Boot Flash
#pragma config UPLLEN   = ON        // USB PLL Enabled
#pragma config UPLLIDIV = DIV_2         // USB PLL Input Divider
#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_2, FWDTEN = OFF
#pragma config POSCMOD = HS, FNOSC = PRIPLL, FPBDIV = DIV_1
#pragma config BWP = OFF

#define SYS_FREQ (80000000L)
#define PB_DIV                 8
#define PRESCALE               256
#define TOGGLES_PER_SEC         1
#define TOGGLES_PER_100MS       10
#define TOGGLES_PER_10MS        100
#define T4_TICK               (SYS_FREQ/PB_DIV/PRESCALE/TOGGLES_PER_100MS)
#define T5_TICK               (SYS_FREQ/PB_DIV/PRESCALE/TOGGLES_PER_SEC)

 //LED
#define LED1_ON()     mPORTDSetBits(BIT_1);
#define LED1_OFF()    mPORTDClearBits(BIT_1);
#define LED2_ON()     mPORTDSetBits(BIT_2);
#define LED2_OFF()    mPORTDClearBits(BIT_2);
#define LED3_ON()     mPORTDSetBits(BIT_3);
#define LED3_OFF()    mPORTDClearBits(BIT_3);
#define LED4_ON()     mPORTCSetBits(BIT_1);
#define LED4_OFF()    mPORTCClearBits(BIT_1);
#define LED5_ON()     mPORTCSetBits(BIT_2);
#define LED5_OFF()    mPORTCClearBits(BIT_2);
#if defined(TRANSPORT_LAYER_ETH)
	#pragma config FMIIEN = OFF, FETHIO = OFF	// external PHY in RMII/alternate configuration	
#endif

#if defined(__PIC32MX1XX_2XX__)
    // For PIC32MX1xx, PIC32MX2xx devices there are jumpers on PIM to choose from PGx1/PGx2.
    #pragma config ICESEL = ICS_PGx1    // ICE pins configured on PGx1 (PGx2 is multiplexed with USB D+ and D- pins).
    // For PIC32MX1xx, PIC32MX2xx devices the output divisor is set to 2 to produce max 40MHz clock.
    #pragma config FPLLODIV = DIV_2         // PLL Output Divider: Divide by 2
#elif defined(__PIC32MX3XX_7XX__)
    // For PIC32MX3xx, PIC32MX4xx, PIC32MX5xx, PIC32MX6xx and PIC32MX7xx 
    // devices the ICE connection is on PGx2. .
    #pragma config ICESEL = ICS_PGx2    // ICE pins configured on PGx2, Boot write protect OFF.
    //For PIC32MX3xx, PIC32MX4xx, PIC32MX5xx, PIC32MX6xx and PIC32MX7xx devices, 
    //the output divisor is set to 1 to produce max 80MHz clock.
    #pragma config FPLLODIV = DIV_1         // PLL Output Divider: Divide by 1
#endif

static const unsigned char	LED_pattern[]=
{
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
        0x02,	0x02,	0x02,	0x02,	0x02,	0x02,	0x02,	0x02,
        0x00,	0x00,	0x00,	0x02,	0x00,	0x00,	0x00,	0x00,
        0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
        0x00,	0x00,	0x00,	0x00,	0x0f,	0x00,	0x00,	0x00,
        0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
};

/************************************
 type defs
**************************************/

typedef struct
{
    UINT8 *start;
    UINT8 len;
    UINT8 status;
}T_REC;

typedef struct 
{
	UINT8 RecDataLen;
	DWORD_VAL Address;
	UINT8 RecType;
	UINT8* Data;
	UINT8 CheckSum;	
	DWORD_VAL ExtSegAddress;
	DWORD_VAL ExtLinAddress;
}T_HEX_RECORD;	
            
   volatile BOOL deviceAttached;
/////////////////////////////////////////////////////////////////////////////
//                            LOCAL PROTOTYPES
/////////////////////////////////////////////////////////////////////////////

WORD            Msgsss1(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg);
WORD            Msgsss2(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg);
WORD            Msgsss3(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg);
WORD            Msgsss4(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg);
WORD            Msgsss5(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg);
WORD            Msgsss6(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg);
WORD            Msgsss7(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg);
WORD            Msgsss8(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg);
void sss6Drawcallback(void);
void sss2Drawcallback(void);
void sss3Drawcallback(void);
void sss4Drawcallback(void);
void sss5Drawcallback(void);
/* */
/////////////////////////////////////////////////////////////////////////////
//                            DEMO STATES
/////////////////////////////////////////////////////////////////////////////
typedef enum
{
     CREATE_sss1          = 0,
     DISPLAY_sss1,
     CREATE_sss2,
     DISPLAY_sss2,
     CREATE_sss3,
     DISPLAY_sss3,
     CREATE_sss4,
     DISPLAY_sss4,
     CREATE_sss5,
     DISPLAY_sss5,
      CREATE_sss6,
     DISPLAY_sss6,
     CREATE_sss7,
     DISPLAY_sss7,
     CREATE_sss8,
     DISPLAY_sss8,
             download,
             
 //ACCL start
    
    CREATE_ACCL,
    DISPLAY_ACCL,
    BOX_DRAW_ACCL
    
 //ACCL end



} SCREEN_STATES;
typedef enum
{
            no_check_usb=0,
            check_usb,
            write_usb_detect,
            write_usb,
                    read_usb,
            usb_attach,

} USB_STATES;


/////////////////////////////////////////////////////////////////////////////
//                       GLOBAL VARIABLES FOR DEMO
/////////////////////////////////////////////////////////////////////////////
 SCREEN_STATES   screenState = CREATE_sss1;
  USB_STATES     usbState = no_check_usb;
 //Graphics prototypes start
/////////////////////////////////////////////////////////////////////////////
//                            LOCAL PROTOTYPES
/////////////////////////////////////////////////////////////////////////////
void            TickInit(void);                 // starts tick counter
WORD ExternalMemoryCallback(IMAGE_EXTERNAL *, LONG , WORD , void *);
void InitializeBoard(void);
void ConverAsciiToHex(UINT8* asciiRec, UINT8* hexRec);
void EraseFlash(void);
void WriteHexRecord2Flash(UINT8* HexRecord);
//ACCL start
/////////////////////////////////////////////////////////////////////////////
//                              OBJECT'S IDs
/////////////////////////////////////////////////////////////////////////////
#define ID_WINDOW1          140
#ifndef  ACCEL_BMA150_RANGE_2G
#define     ACCEL_BMA150_RANGE_2G       (0)
#endif
#ifndef  ACCEL_BMA150_RANGE_4G
#define     ACCEL_BMA150_RANGE_4G       (1)
#endif
#ifndef  ACCEL_BMA150_RANGE_8G
#define     ACCEL_BMA150_RANGE_8G       (2)
#endif
#define WAIT_UNTIL_FINISH(x)    while(!x)
/////////////////////////////////////////////////////////////////////////////
//                            LOCAL PROTOTYPES
/////////////////////////////////////////////////////////////////////////////
void 			    StartScreen();                                 // draws intro screen
void                        CreatePage(XCHAR *pText);   // creates the navigation buttons and window for each screen
void                        CreateMenu(void);        // creates buttons demo screen
WORD                        MsgMenu(WORD objMsg, OBJ_HEADER *pObj);                  // processes messages for buttons demo screen
void                        CreateAccelerometer(void);                          // scrolling graph demo screen
WORD                        PanelAccelerometer(void);                           // draws box for the scrolling graph
void                        GraphAccelerometer(void);                           // draws scrolling graph graph
WORD                        GetACCLSamples(WORD number);                         // adds sample from Accelerometer channel into temporary buffer
WORD                        MsgAccelerometer(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg);    // processes messages for ECG demo screen
void                        ErrorTrap(XCHAR *message);                                  // outputs text message on screen and stop execution
void                        TickInit(void);                                             // starts tick counter

/////////////////////////////////////////////////////////////////////////////
//                            IMAGES USED
/////////////////////////////////////////////////////////////////////////////
// iinternal flash image
extern const BITMAP_FLASH   intro;
extern const BITMAP_FLASH   mchpLogo;
extern const BITMAP_FLASH   mchpIcon0;
extern const BITMAP_FLASH   mchpIcon;

/////////////////////////////////////////////////////////////////////////////
//                             FONTS USED
/////////////////////////////////////////////////////////////////////////////
extern const FONT_FLASH		GOLFontDefault;
extern const FONT_FLASH     GOLMediumFont;                 // default GOL font
extern const FONT_FLASH     GOLSmallFont;                   // small font

/////////////////////////////////////////////////////////////////////////////
//                            COLORS USED
/////////////////////////////////////////////////////////////////////////////
#define GRAY20      RGB565CONVERT(51, 51, 51)
#define GRAY40      RGB565CONVERT(102, 102, 102)
#define GRAY80      RGB565CONVERT(204, 204, 204)
#define GRAY90      RGB565CONVERT(229, 229, 229)
#define GRAY95      RGB565CONVERT(242, 242, 242)
#define RED4        RGB565CONVERT(139, 0, 0)
#define FIREBRICK1  RGB565CONVERT(255, 48, 48)
#define DARKGREEN   RGB565CONVERT(0, 100, 0)
#define PALEGREEN   RGB565CONVERT(152, 251, 152)
#define LIGHTYELLOW RGB565CONVERT(238, 221, 130)
#define GOLD        RGB565CONVERT(255, 215, 0)
#define DARKORANGE  RGB565CONVERT(255, 140, 0)
//Accelerometer States
typedef enum
{
	NONE       = 0,
    X          = 1,
    Y          = 2,
    Z          = 4,
    XY         = 3,
    XZ         = 5,
	YZ         = 6,
	XYZ        = 7

} Accelerometer_Axes;

/////////////////////////////////////////////////////////////////////////////
//                       Demo Strings
/////////////////////////////////////////////////////////////////////////////

const XCHAR TouchScreenStr[] = "Touch Screen to continue";
const XCHAR	AcclMenuStr[]    = "Accelerometer Menu";
const XCHAR AcclDemoStr[]    = "Accelerometer Demo";
const XCHAR Str_X[]			 = "X Axis";
const XCHAR Str_Y[]			 = "Y Axis";
const XCHAR Str_Z[]			 = "Z Axis";
const XCHAR Str_2G[]         = "2G";
const XCHAR Str_4G[]         = "4G";
const XCHAR Str_8G[]         = "8G";
const XCHAR RunButtonStr[]   = "Run";
const XCHAR BackButtonStr[]  = "<<";
XCHAR temperature_str[20];
/////////////////////////////////////////////////////////////////////////////
//                       GLOBAL VARIABLES FOR DEMO
/////////////////////////////////////////////////////////////////////////////
Accelerometer_Axes	Axes 				= XYZ;
unsigned char Accl_Sensivity_Select		= ACCEL_BMA150_RANGE_8G;
SCREEN_STATES   	prevRefreshState 	= CREATE_ACCL;// used to mark the start of the previous screen
SCREEN_STATES   	prevState 			= CREATE_ACCL; // used to mark state where time setting was called
GOL_SCHEME      	*altScheme;                                 // alternative style scheme
GOL_SCHEME      	*navScheme;                                 // style scheme for the navigation
volatile DWORD  	tick 				= 0;                                   // tick counter
volatile DWORD          prevTick = 0;                           // keeps previous value of tick
ACCEL_INIT  accel_init;
ACCEL_DATA  accel_x, accel_y, accel_z;
unsigned char temperature;        //Variable used to store the temperature values read from BMA150 Accelerometer/Temperature Sensor
// Accelerometer Graph shift
#define ACCL_MOVE_DELTA  2
// Accelerometer circular buffer size
#define ACCL_BUFFER_SIZE 320
// Accelerometer data circular buffer
SHORT           acclBuffer_X[320];
SHORT           acclBuffer_Y[320];
SHORT           acclBuffer_Z[320];
// Temporary buffer for graph demo screens
SHORT           tempBuffer_X[10];
SHORT           tempBuffer_Y[10];
SHORT           tempBuffer_Z[10];
 //ACCL end
/******************************************************************************
Macros used in this file
*******************************************************************************/
#define SWITCH_PRESSED 0
#define DEV_CONFIG_REG_BASE_ADDRESS 0x9FC02FF0
#define DEV_CONFIG_REG_END_ADDRESS   0x9FC02FFF
#define DATA_RECORD 		0
#define END_OF_FILE_RECORD 	1
#define EXT_SEG_ADRS_RECORD 2
#define EXT_LIN_ADRS_RECORD 4

/******************************************************************************
Global Variables
*******************************************************************************/
FSFILE * myFile;
BYTE myData[512];
size_t numBytes;
UINT pointer = 0;
UINT readBytes;
UINT8 asciiBuffer[1024];
UINT8 asciiRec[200];
UINT8 hexRec[100];
T_REC record;
#define REC_FLASHED 0
#define REC_NOT_FOUND 1
#define REC_FOUND_BUT_NOT_FLASHED 2
#include "GenericTypeDefs.h"
/****************************************************************************
Function prototypes
*****************************************************************************/
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec);
void InitializeBoard(void);
BOOL CheckTrigger(void);
void JumpToApp(void);
BOOL ValidAppPresent(void);
/********************************************************************
* Function: 	main()
*
* Precondition: 
*
* Input: 		None.
*
* Output:		None.
*
* Side Effects:	None.
*
* Overview: 	Main entry function. If there is a trigger or 
*				if there is no valid application, the device 
*				stays in firmware upgrade mode.
*
*			
* Note:		 	None.
********************************************************************/
int main(void)
{
    volatile UINT i;
    int	dmaChn=1;   // The DMA channel to use
     XCHAR* ttt="Image Finded,Downloading!";
    DmaChnOpen(dmaChn, 0, DMA_OPEN_AUTO);
    // Set the transfer parameters: source & destination address, source & destination size, number of bytes per event
    DmaChnSetTxfer(dmaChn, LED_pattern, (void*)&LATC, sizeof(LED_pattern), 1, 1);
    // Set the transfer event control: what event is to start the DMA transfer
    DmaChnSetEventControl(dmaChn, DMA_EV_START_IRQ(_TIMER_3_IRQ));
    // Once we configured the DMA channel we can enable it
    // Now it's ready and waiting for an event to occur...
    DmaChnEnable(dmaChn);
    mPORTDSetPinsDigitalOut(BIT_3 | BIT_2 | BIT_1);
    mPORTCSetPinsDigitalOut(BIT_2 | BIT_1);
    soft_rtcc_init();
    // Setup configuration
    (void)SYSTEMConfig(SYS_FREQ, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
    INTEnableSystemMultiVectoredInt();
    //ACCL start
    navScheme 						= GOLCreateScheme();      // alternative scheme for the navigate buttons
    altScheme 						= GOLCreateScheme();      // create alternative 1 style scheme
    /* for Truly display */
    altScheme->Color0 				= RGB565CONVERT(0x4C, 0x8E, 0xFF);
    altScheme->Color1 				= RGB565CONVERT(0xFF, 0xBB, 0x4C);
    altScheme->EmbossDkColor 		= RGB565CONVERT(0x1E, 0x00, 0xE5);
    altScheme->EmbossLtColor 		= RGB565CONVERT(0xA9, 0xDB, 0xEF);
    altScheme->ColorDisabled 		= RGB565CONVERT(0xD4, 0xE1, 0xF7);
    altScheme->TextColor1 			= BRIGHTBLUE;
    altScheme->TextColor0 			= BRIGHTBLUE;
    altScheme->pFont 				= (void *)&Gentium_16;
    navScheme->pFont 				= (void *)&Gentium_16;
    navScheme->Color0 				= RGB565CONVERT(0x4C, 0x8E, 0xFF);
    navScheme->Color1 				= RGB565CONVERT(0xFF, 0xBB, 0x4C);
    navScheme->EmbossDkColor 		= RGB565CONVERT(0x1E, 0x00, 0xE5);
    navScheme->EmbossLtColor 		= RGB565CONVERT(0xA9, 0xDB, 0xEF);
	//ACCL end    
   if(!CheckTrigger() && ValidAppPresent())
	{
       DmaChnDisable(1);
       CloseTimer5();
       CloseTimer4();
       // This means the switch is not pressed. Jump
        // directly to the application
        JumpToApp();
       }
    OpenTimer4(T4_ON | T4_PS_1_256, T4_TICK);
    ConfigIntTimer4(T4_INT_ON | T4_INT_PRIOR_1);
    OpenTimer5(T5_ON | T5_PS_1_256, T5_TICK);
    ConfigIntTimer5(T5_INT_ON | T5_INT_PRIOR_3);
    deviceAttached = FALSE;
    //Initialize the stack
    USBInitialize(0);
   //Graphics instruction start
    GOL_MSG msg;                    // GOL message structure to interact with GOL
    InitializeBoard();
    GDDDemoCreateFirstScreen();
    //Graphics instruction end
    // Accelerometer Initialization
    accel_init.sourceClock  = GetPeripheralClock();
    accel_init.dataRate     = 100000;
    ACCELInitialize(&accel_init, ACCEL_BMA150_RANGE_2G);
    accel_x = 0;
    accel_y = 0;
    accel_z = 0;    
// Read the temperature in degree celsius from the BMA150 Accelerometer/Temperature Sensor
	while (	ACCELGetTemperature(&temperature)== ACCEL_INVALID);
        sprintf((char *)temperature_str, "Temp: %dC", temperature);  // Convert the temperature readings into a string to display
        #ifdef ACCEL_USE_POLLING
        ACCELTask();
        #endif
        if(ACCELGetXYZAxis(&accel_x, &accel_y, &accel_z) == ACCEL_VALID){

        };     
   //Accelerate end

    while(1)
    {
         if(GOLDraw())               // Draw GOL object
        {
            TouchGetMsg(&msg);      // Get message from touch screen

            #if (NUM_GDD_SCREENS > 1)
			// GDD Readme:
			// The following line of code allows a GDD user to touch the touchscreen
			// to cycle through different static screens for viewing. This is useful as a
			// quick way to view how each screen looks on the physical target hardware.
			// This line of code should eventually be commented out for actual development.
			// Also note that widget/object names can be found in GDD_Screens.h
			//if(msg.uiEvent == EVENT_RELEASE) GDDDemoNextScreen();
			#endif

            GOLMsg(&msg);           // Process message
        }
        //Graphics instruction end
        //if thumbdrive is plugged in
     switch(usbState)
     {
        case no_check_usb :
         Nop();
         break;
         
         case check_usb :
         USBTasks();
         if(USBHostMSDSCSIMediaDetect())
        {
            //now a device is attached
            //See if the device is attached and in the right format
            if(FSInit())
            {
                DmaChnDisable(dmaChn);
                CloseTimer5();
                CloseTimer4();
                // Open the hex file
                myFile = FSfopen("image.hex","r");	
                if(myFile != NULL)// Make sure the file is present..
                {
	             
                 StSetText((STATICTEXT *)GOLFindObject(STE_95),((XCHAR*)ttt));
                 SetState((STATICTEXT *)GOLFindObject(STE_95), ST_UPDATE);
                 EraseFlash();
                    // Initialize the state-machine to read the records.
                    record.status = REC_NOT_FOUND;

                while(!CheckTrigger())
                {
                if(GOLDraw())               // Draw GOL object
            {
            TouchGetMsg(&msg);      // Get message from touch screen

            #if (NUM_GDD_SCREENS > 1)
			// GDD Readme:
			// The following line of code allows a GDD user to touch the touchscreen
			// to cycle through different static screens for viewing. This is useful as a
			// quick way to view how each screen looks on the physical target hardware.
			// This line of code should eventually be commented out for actual development.
			// Also note that widget/object names can be found in GDD_Screens.h
			//if(msg.uiEvent == EVENT_RELEASE) GDDDemoNextScreen();
			#endif

            GOLMsg(&msg);           // Process message
        }
                }
           while(1)
                    {
	                    // Call USB tasks.
                        USBTasks();
                        // Blink LED
        		//		BlinkLED();
                        // For a faster read, read 512 bytes at a time and buffer it.
                        readBytes = FSfread((void *)&asciiBuffer[pointer],1,512,myFile);
                        
                        if(readBytes == 0)
                        {
                            // Nothing to read. Come out of this loop
                            // break;
                            // Jump to start of application
                            // Disable all enabled interrupts (only USB)
                            // before jumping to the application code.
                            
                            FSfclose(myFile);
                            //IEC5bits.USB1IE = 0;
                            JumpToApp();
                        }

                        for(i = 0; i < (readBytes + pointer); i ++)
                        {
	                        // This state machine seperates-out the valid hex records from the read 512 bytes.
                            switch(record.status)
                            {
                                case REC_FLASHED:
                                case REC_NOT_FOUND:
                                    if(asciiBuffer[i] == ':')
                                    {
	                                    // We have a record found in the 512 bytes of data in the buffer.
                                        record.start = &asciiBuffer[i];
                                        record.len = 0;
                                        record.status = REC_FOUND_BUT_NOT_FLASHED;
                                    }
                                    break;
                                case REC_FOUND_BUT_NOT_FLASHED:
                                    if((asciiBuffer[i] == 0x0A) || (asciiBuffer[i] == 0xFF))
                                    {
	                                    // We have got a complete record. (0x0A is new line feed and 0xFF is End of file)
                                        // Start the hex conversion from element
                                        // 1. This will discard the ':' which is
                                        // the start of the hex record.
                                        ConvertAsciiToHex(&record.start[1],hexRec);
                                        WriteHexRecord2Flash(hexRec);
                                        record.status = REC_FLASHED;
                                    }
                                    break;
                            }
                            // Move to next byte in the buffer.
                            record.len ++;
                        }

                        if(record.status == REC_FOUND_BUT_NOT_FLASHED)
                        {
	                        // We still have a half read record in the buffer. The next half part of the record is read 
	                        // when we read 512 bytes of data from the next file read. 
                            memcpy(asciiBuffer, record.start, record.len);
                            pointer = record.len;
                            record.status = REC_NOT_FOUND;
                        }
                        else
                        {
                            pointer = 0;
                        }
                    }//while(1)




                }
            }//if(FSInit())
        }//if(USBHostMSDSCSIMediaDetect())
         break;
         case write_usb_detect:

             if((tick - prevTick) >500)
    {
        while (ACCELGetTemperature(&temperature)== ACCEL_INVALID);
        //sprintf((char *)temperature_str, "Temp: %dC, x = %d, y = %d, z = %d", temperature,accel_x,accel_y,accel_z);  // Convert the temperature readings into a string to display
        #ifdef ACCEL_USE_POLLING
        ACCELTask();
        #endif
        if(ACCELGetXYZAxis(&accel_x, &accel_y, &accel_z) == ACCEL_VALID){};
        prevTick = tick;

        sprintf((char *)temperature_str, "%d         %d         %d         %dC", accel_x,accel_y,accel_z,temperature);  // Convert the temperature readings into a string to display
                 StSetText((STATICTEXT *)GOLFindObject(STE_72),((XCHAR*)temperature_str));
                 SetState((STATICTEXT *)GOLFindObject(STE_72), ST_UPDATE);
      }

        USBTasks();
         if(USBHostMSDSCSIMediaDetect())
        {
            //now a device is attached
            //See if the device is attached and in the right format
            SetState(((BUTTON*)(GOLFindObject(BTN_91))), (BTN_DRAW));
            SetState(((BUTTON*)(GOLFindObject(BTN_90))), (BTN_DRAW));
         //usbState=no_check_usb;
         }
         else
         {
             SetState(((BUTTON*)(GOLFindObject(BTN_91))), (BTN_HIDE));
            SetState(((BUTTON*)(GOLFindObject(BTN_90))), (BTN_HIDE));
         }
          break;

         case write_usb:
               // Read the temperature in degree celsius from the BMA150 Accelerometer/Temperature Sensor
	while (	ACCELGetTemperature(&temperature)== ACCEL_INVALID);
        //sprintf((char *)temperature_str, "Temp: %dC, x = %d, y = %d, z = %d", temperature,accel_x,accel_y,accel_z);  // Convert the temperature readings into a string to display
        #ifdef ACCEL_USE_POLLING
        ACCELTask();
        #endif
        if(ACCELGetXYZAxis(&accel_x, &accel_y, &accel_z) == ACCEL_VALID){};

        sprintf((char *)temperature_str, "%d         %d         %d         %dC", accel_x,accel_y,accel_z,temperature);  // Convert the temperature readings into a string to display
        StSetText((STATICTEXT *)GOLFindObject(STE_72),((XCHAR*)temperature_str));
                 SetState((STATICTEXT *)GOLFindObject(STE_72), ST_UPDATE);
        USBTasks();
        if(USBHostMSDSCSIMediaDetect())
        {
            //now a device is attached
            //See if the device is attached and in the right format
            if(FSInit())
            {
                //Opening a file in mode "w" will create the file if it doesn't
                //  exist.  If the file does exist it will delete the old file
                //  and create a new one that is blank.
                myFile = FSfopen("Peter.txt","w");

                //Write some data to the new file.
                FSfwrite(temperature_str,1,40,myFile);


                //Always make sure to close the file so that the data gets
                //  written to the drive.
                FSfclose(myFile);
            }
        }
        usbState=no_check_usb;


               Nop();
               break;
         case read_usb:

        USBTasks();
        if(USBHostMSDSCSIMediaDetect())
        {
            //now a device is attached
            //See if the device is attached and in the right format
            if(FSInit())
            {
              myFile = FSfopen("PETER.txt","r");
              FSfread((void *)&temperature_str,1,40,myFile);
              FSfclose(myFile);
            }
        }
             StSetText((STATICTEXT *)GOLFindObject(STE_72),((XCHAR*)temperature_str));
                 SetState((STATICTEXT *)GOLFindObject(STE_72), ST_UPDATE);
                usbState=no_check_usb;
        Nop();
             break;


     } //switch
    }//while(1)
       

}

/********************************************************************
* Function: 	CheckTrigger()
*
* Precondition: 
*
* Input: 		None.
*
* Output:		TRUE: If triggered
				FALSE: No trigger
*
* Side Effects:	None.
*
* Overview: 	Checks if there is a trigger to enter 
				firmware upgrade mode.
*
*			
* Note:		 	None.
********************************************************************/
BOOL CheckTrigger(void)
{
	BOOL SwitchStatus;
	SwitchStatus = PORTBbits.RB15;
	if(SwitchStatus == SWITCH_PRESSED)
	{
		// Switch is pressed
		return TRUE;		
	}	
	else
	{
		// Switch is not pressed.
		return FALSE;	
	}	
}	


/********************************************************************
* Function: 	JumpToApp()
*
* Precondition: 
*
* Input: 		None.
*
* Output:		
*
* Side Effects:	No return from here.
*
* Overview: 	Jumps to application.
*
*			
* Note:		 	None.
********************************************************************/
void JumpToApp(void)
{	
	void (*fptr)(void);
	fptr = (void (*)(void))USER_APP_RESET_ADDRESS;
	fptr();
}	
/********************************************************************
* Function: 	ConvertAsciiToHex()
*
* Precondition: 
*
* Input: 		Ascii buffer and hex buffer.
*
* Output:		
*
* Side Effects:	No return from here.
*
* Overview: 	Converts ASCII to Hex.
*
*			
* Note:		 	None.
********************************************************************/
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec)
{
	UINT8 i = 0;
	UINT8 k = 0;
	UINT8 hex;
	
	
	while((asciiRec[i] >= 0x30) && (asciiRec[i] <= 0x66))
	{
		// Check if the ascci values are in alpha numeric range.
		
		if(asciiRec[i] < 0x3A)
		{
			// Numerical reperesentation in ASCII found.
			hex = asciiRec[i] & 0x0F;
		}
		else
		{
			// Alphabetical value.
			hex = 0x09 + (asciiRec[i] & 0x0F);						
		}
	
		// Following logic converts 2 bytes of ASCII to 1 byte of hex.
		k = i%2;
		
		if(k)
		{
			hexRec[i/2] |= hex;
			
		}
		else
		{
			hexRec[i/2] = (hex << 4) & 0xF0;
		}	
		i++;		
	}		
	
}
// Do not change this
#define FLASH_PAGE_SIZE 0x1000
/********************************************************************
* Function: 	EraseFlash()
*
* Precondition: 
*
* Input: 		None.
*
* Output:		
*
* Side Effects:	No return from here.
*
* Overview: 	Erases Flash (Block Erase).
*
*			
* Note:		 	None.
********************************************************************/
void EraseFlash(void)
{
	void * pFlash;
    UINT result;
    INT i;

    pFlash = (void*)APP_FLASH_BASE_ADDRESS;									
    for( i = 0; i < ((APP_FLASH_END_ADDRESS - APP_FLASH_BASE_ADDRESS + 1)/FLASH_PAGE_SIZE); i++ )
    {
	     result = NVMemErasePage( pFlash + (i*FLASH_PAGE_SIZE) );
        // Assert on NV error. This must be caught during debug phase.

        if(result != 0)
        {
           // We have a problem. This must be caught during the debug phase.
            while(1);
        } 
        // Blink LED to indicate erase is in progress.
 #if defined(__PIC32MX3XX_7XX__)
        mLED = mLED ^ 1;
 #endif
    }			           	     
}



/********************************************************************
* Function: 	WriteHexRecord2Flash()
*
* Precondition: 
*
* Input: 		None.
*
* Output:		
*
* Side Effects:	No return from here.
*
* Overview: 	Writes Hex Records to Flash.
*
*			
* Note:		 	None.
********************************************************************/
void WriteHexRecord2Flash(UINT8* HexRecord)
{
	static T_HEX_RECORD HexRecordSt;
	UINT8 Checksum = 0;
	UINT8 i;
	UINT WrData;
	//UINT RdData;
	void* ProgAddress;
	UINT result;
		
	HexRecordSt.RecDataLen = HexRecord[0];
	HexRecordSt.RecType = HexRecord[3];	
	HexRecordSt.Data = &HexRecord[4];	
	
	// Hex Record checksum check.
	for(i = 0; i < HexRecordSt.RecDataLen + 5; i++)
	{
		Checksum += HexRecord[i];
	}	
	
    if(Checksum != 0)
    {
	    //Error. Hex record Checksum mismatch.
	    //Indicate Error by switching ON all LEDs.
	    Error();
	    // Do not proceed further.
	    while(1);
	} 
	else
	{
		// Hex record checksum OK.
		switch(HexRecordSt.RecType)
		{
			case DATA_RECORD:  //Record Type 00, data record.
				HexRecordSt.Address.byte.MB = 0;
					HexRecordSt.Address.byte.UB = 0;
					HexRecordSt.Address.byte.HB = HexRecord[1];
					HexRecordSt.Address.byte.LB = HexRecord[2];
					
					// Derive the address.
					HexRecordSt.Address.Val = HexRecordSt.Address.Val + HexRecordSt.ExtLinAddress.Val + HexRecordSt.ExtSegAddress.Val;
							
					while(HexRecordSt.RecDataLen) // Loop till all bytes are done.
					{
											
						// Convert the Physical address to Virtual address. 
						ProgAddress = (void *)PA_TO_KVA0(HexRecordSt.Address.Val);
						
						// Make sure we are not writing boot area and device configuration bits.
						if(((ProgAddress >= (void *)APP_FLASH_BASE_ADDRESS) && (ProgAddress <= (void *)APP_FLASH_END_ADDRESS))
						   && ((ProgAddress < (void*)DEV_CONFIG_REG_BASE_ADDRESS) || (ProgAddress > (void*)DEV_CONFIG_REG_END_ADDRESS)))
						{
							if(HexRecordSt.RecDataLen < 4)
							{
								
								// Sometimes record data length will not be in multiples of 4. Appending 0xFF will make sure that..
								// we don't write junk data in such cases.
								WrData = 0xFFFFFFFF;
								memcpy(&WrData, HexRecordSt.Data, HexRecordSt.RecDataLen);	
							}
							else
							{	
								memcpy(&WrData, HexRecordSt.Data, 4);
							}		
							// Write the data into flash.	
							result = NVMemWriteWord(ProgAddress, WrData);	
							// Assert on error. This must be caught during debug phase.		
							if(result != 0)
							{
    							while(1);
    						}									
						}	
						
						// Increment the address.
						HexRecordSt.Address.Val += 4;
						// Increment the data pointer.
						HexRecordSt.Data += 4;
						// Decrement data len.
						if(HexRecordSt.RecDataLen > 3)
						{
							HexRecordSt.RecDataLen -= 4;
						}	
						else
						{
							HexRecordSt.RecDataLen = 0;
						}	
					}
					break;
			
			case EXT_SEG_ADRS_RECORD:  // Record Type 02, defines 4th to 19th bits of the data address.
			    HexRecordSt.ExtSegAddress.byte.MB = 0;
				HexRecordSt.ExtSegAddress.byte.UB = HexRecordSt.Data[0];
				HexRecordSt.ExtSegAddress.byte.HB = HexRecordSt.Data[1];
				HexRecordSt.ExtSegAddress.byte.LB = 0;
				// Reset linear address.
				HexRecordSt.ExtLinAddress.Val = 0;
				break;
				
			case EXT_LIN_ADRS_RECORD:   // Record Type 04, defines 16th to 31st bits of the data address. 
				HexRecordSt.ExtLinAddress.byte.MB = HexRecordSt.Data[0];
				HexRecordSt.ExtLinAddress.byte.UB = HexRecordSt.Data[1];
				HexRecordSt.ExtLinAddress.byte.HB = 0;
				HexRecordSt.ExtLinAddress.byte.LB = 0;
				// Reset segment address.
				HexRecordSt.ExtSegAddress.Val = 0;
				break;
				
			case END_OF_FILE_RECORD:  //Record Type 01, defines the end of file record.
				HexRecordSt.ExtSegAddress.Val = 0;
				HexRecordSt.ExtLinAddress.Val = 0;
				// Disable any interrupts here before jumping to the application.
				//IEC1bits.USBIE = 0;
				USBDisableInterrupts();
				JumpToApp();
				break;
				
			default: 
				HexRecordSt.ExtSegAddress.Val = 0;
				HexRecordSt.ExtLinAddress.Val = 0;
				break;
		}		
	}	
		
}	

/********************************************************************
* Function: 	ValidAppPresent()
*
* Precondition: 
*
* Input: 		None.
*
* Output:		TRUE: If application is valid.
*
* Side Effects:	None.
*
* Overview: 	Logic: Check application vector has 
				some value other than "0xFFFFFF"
*
*			
* Note:		 	None.
********************************************************************/
BOOL ValidAppPresent(void)
{
	volatile UINT32 *AppPtr;
	
	AppPtr = (UINT32*)USER_APP_RESET_ADDRESS;

	if(*AppPtr == 0xFFFFFFFF)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}			

/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event,
                void *data, DWORD size )

  Summary:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.

  Description:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.  If the application is able to handle the event, it
    returns TRUE.  Otherwise, it returns FALSE.

  Precondition:
    None

  Parameters:
    BYTE address    - Address of device where event occurred
    USB_EVENT event - Identifies the event that occured
    void *data      - Pointer to event-specific data
    DWORD size      - Size of the event-specific data

  Return Values:
    TRUE    - The event was handled
    FALSE   - The event was not handled

  Remarks:
    The application may also implement an event handling routine if it
    requires knowledge of events.  To do so, it must implement a routine that
    matches this function signature and define the USB_HOST_APP_EVENT_HANDLER
    macro as the name of that function.
  ***************************************************************************/

BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )
{
    switch( event )
    {
        case EVENT_VBUS_REQUEST_POWER:
            // The data pointer points to a byte that represents the amount of power
            // requested in mA, divided by two.  If the device wants too much power,
            // we reject it.
            return TRUE;

        case EVENT_VBUS_RELEASE_POWER:
            // Turn off Vbus power.
            // The PIC24F with the Explorer 16 cannot turn off Vbus through software.

            //This means that the device was removed
            //deviceAttached = FALSE;
            return TRUE;
            break;

        case EVENT_HUB_ATTACH:
            return TRUE;
            break;

        case EVENT_UNSUPPORTED_DEVICE:
            return TRUE;
            break;

        case EVENT_CANNOT_ENUMERATE:
            //UART2PrintString( "\r\n***** USB Error - cannot enumerate device *****\r\n" );
            return TRUE;
            break;

        case EVENT_CLIENT_INIT_ERROR:
            //UART2PrintString( "\r\n***** USB Error - client driver initialization error *****\r\n" );
            return TRUE;
            break;

        case EVENT_OUT_OF_MEMORY:
            //UART2PrintString( "\r\n***** USB Error - out of heap memory *****\r\n" );
            return TRUE;
            break;

        case EVENT_UNSPECIFIED_ERROR:   // This should never be generated.
            //UART2PrintString( "\r\n***** USB Error - unspecified *****\r\n" );
            return TRUE;
            break;

        default:
            break;
    }

    return FALSE;
}



/*********************End of File************************************/

//Graphics program start
/////////////////////////////////////////////////////////////////////////////
// Function: WORD GOLMsgCallback(WORD objMsg, OBJ_HEADER* pObj, GOL_MSG* pMsg)
// Input: objMsg - translated message for the object,
//        pObj - pointer to the object,
//        pMsg - pointer to the non-translated, raw GOL message
// Output: if the function returns non-zero the message will be processed by default
// Overview: it's a user defined function. GOLMsg() function calls it each

//           time the valid message for the object received
/////////////////////////////////////////////////////////////////////////////
WORD GOLMsgCallback(WORD objMsg, OBJ_HEADER *pObj, GOL_MSG *pMsg)
{
   
    WORD    objectID;

    objectID = GetObjID(pObj);

    GDDDemoGOLMsgCallback(objMsg, pObj, pMsg);

    // Add additional code here...
switch(screenState)
 {
        case  DISPLAY_sss1 :
            return(Msgsss1(objMsg,pObj,pMsg));
            //break;
        case  DISPLAY_sss2 :
            return(Msgsss2(objMsg,pObj,pMsg));
           // break;
    case  DISPLAY_sss3 :
            return(Msgsss3(objMsg,pObj,pMsg));
           // break;
    case  DISPLAY_sss4 :
            return(Msgsss4(objMsg,pObj,pMsg));
           // break;
    case  DISPLAY_sss5 :
            return(Msgsss5(objMsg,pObj,pMsg));
           // break;

    case  DISPLAY_sss6 :
        return(Msgsss6(objMsg,pObj,pMsg));
            //break;
    case  DISPLAY_sss7 :
            return(Msgsss7(objMsg,pObj,pMsg));
    case  DISPLAY_sss8 :
            return(Msgsss8(objMsg,pObj,pMsg));
    case DISPLAY_ACCL:
            return (MsgAccelerometer(objMsg, pObj,pMsg));
    case download:
        while(1);
    default:
            return (1);
     //break;
    }
    return (1);
    }

/////////////////////////////////////////////////////////////////////////////
// Function: WORD GOLDrawCallback()
// Output: if the function returns non-zero the draw control will be passed to GOL
// Overview: it's a user defined function. GOLDraw() function calls it each
//           time when GOL objects drawing is completed. User drawing should be done here.
//           GOL will not change color, line type and clipping region settings while

//           this function returns zero.
/////////////////////////////////////////////////////////////////////////////
WORD GOLDrawCallback(void)
{
    GDDDemoGOLDrawCallback();

 //ACCL start
   //static DWORD    prevTick = 0;                           // keeps previous value of tick
   
 	int counter;


 //ACCL end
    // Add additional code here...
switch(screenState)
   {
       case CREATE_sss1:
        Createsrc1();
        CreatePrimitivesForsrc1();
        screenState=DISPLAY_sss1;
        return(1);
    case CREATE_sss2:
        Createsrc2();
        CreatePrimitivesForsrc2();
        screenState=DISPLAY_sss2;
        return(1);
    case CREATE_sss3:
        Createsrc3();
        screenState=DISPLAY_sss3;
        return(1);
    case CREATE_sss4:
        Createsrc4();
        screenState=DISPLAY_sss4;
        return(1);
    case CREATE_sss5:
        Createsrc5();
        CreatePrimitivesForsrc5();
        screenState=DISPLAY_sss5;
        return(1);
    case CREATE_sss6:
        Createsrc6();
        CreatePrimitivesForsrc6();
        usbState = write_usb_detect;
        screenState=DISPLAY_sss6;
        return(1);
    case CREATE_sss7:
        Createsrc7();
        screenState=DISPLAY_sss7;
        return(1);
    case CREATE_sss8:
        Createsrc8();
        screenState=DISPLAY_sss8;
        return(1);
    case DISPLAY_sss6:
        sss6Drawcallback();
        return(1);
    case DISPLAY_sss2:
           sss2Drawcallback();
        return(1);
        case DISPLAY_sss3:
            sss3Drawcallback();
            return(1);
            case DISPLAY_sss4:
            sss4Drawcallback();
            return(1);
            case DISPLAY_sss5:
            sss5Drawcallback();
            return(1);

        //ACCL start


        case CREATE_ACCL:
            prevRefreshState = CREATE_ACCL;
            CreateAccelerometer();              			// create window
            screenState = BOX_DRAW_ACCL;         			// switch to next state
            return (1);                         			// draw objects created

        case BOX_DRAW_ACCL:
            if(0 == PanelAccelerometer())  				    // draw box for Accelerometer graph
                return (0);                     			// drawing is not completed, don't pass

            // drawing control to GOL, try it again
			// Clear the buffer contents before plotting
			for(counter = 0; counter < 320; counter++)
			{
				acclBuffer_X[counter] = 0;
				acclBuffer_Y[counter] = 0;
				acclBuffer_Z[counter] = 0;
			}
            screenState = DISPLAY_ACCL;          			// switch to next state
            return (1);                         			// pass drawing control to GOL, redraw objects if needed

        case DISPLAY_ACCL:
            if((tick - prevTick) > 15)
            {
                if(GetACCLSamples(ACCL_MOVE_DELTA))
                    GraphAccelerometer();       			// redraw graph
                prevTick = tick;
            }
            return (1);                         			// redraw objects if needed
         }

         //ACCL end

    return (1);
}


/////////////////////////////////////////////////////////////////////////////
// Function: Timer3 ISR
// Input: none
// Output: none
// Overview: increments tick counter. Tick is approx. 1 ms.
/////////////////////////////////////////////////////////////////////////////
#ifdef __PIC32MX__
    #define __T3_ISR    __ISR(_TIMER_3_VECTOR, ipl4)
#else
    #define __T3_ISR    __attribute__((interrupt, shadow, auto_psv))
#endif

/* */
void __T3_ISR _T3Interrupt(void)
{
    tick++;
    TMR3 = 0;
    // Clear flag
    #ifdef __PIC32MX__
    mT3ClearIntFlag();
    #else
    IFS0bits.T3IF = 0;
    #endif

    TouchDetectPosition();
}

/////////////////////////////////////////////////////////////////////////////
// Function: void TickInit(void)
// Input: none
// Output: none
// Overview: Initilizes the tick timer.
/////////////////////////////////////////////////////////////////////////////

/*********************************************************************
 * Section: Tick Delay
 *********************************************************************/
#define SAMPLE_PERIOD       500 // us
#define TICK_PERIOD			(GetPeripheralClock() * SAMPLE_PERIOD) / 4000000

/* */
void TickInit(void)
{

    // Initialize Timer4
    #ifdef __PIC32MX__
    OpenTimer3(T3_ON | T3_PS_1_8, TICK_PERIOD);
    ConfigIntTimer3(T3_INT_ON | T3_INT_PRIOR_4);
    #else
    TMR3 = 0;
    PR3 = TICK_PERIOD;
    IFS0bits.T3IF = 0;  //Clear flag
    IEC0bits.T3IE = 1;  //Enable interrupt
    T3CONbits.TON = 1;  //Run timer
    #endif

}
/*********************************************************************
* Function: WORD ExternalMemoryCallback(EXTDATA* memory, LONG offset, WORD nCount, void* buffer)
*
* PreCondition: none
*
* Input:  memory - pointer to the bitmap or font external memory structures
*                  (FONT_EXTERNAL or BITMAP_EXTERNAL)
*         offset - data offset
*         nCount - number of bytes to be transferred to the buffer
*         buffer - pointer to the buffer
*
* Output: number of bytes were transferred.
*
* Side Effects: none
*
* Overview: this function must be implemented in application. Graphics Library calls it
*           each time the data from external memory is required. The application must copy
*           required data to the buffer provided.
*
* Note: none
*
********************************************************************/
// If there are several memories in the system they can be selected by IDs.
// In this demo ID for memory chip installed on Graphics PICTail board is assumed to be 0.
#define SST39_MEMORY    0
/* */

WORD ExternalMemoryCallback(IMAGE_EXTERNAL *memory, LONG offset, WORD nCount, void *buffer)
{
    //if(memory->ID == SST39_MEMORY)
    //{

        // Read data requested into buffer provided
        SST25ReadArray(memory->address + offset, // address to read from
        (BYTE *)buffer, nCount);
   // }

    return (nCount);
}

/////////////////////////////////////////////////////////////////////////////
// Function: InitializeBoard()
// Input: none
// Output: none
// Overview: Initializes the hardware components including the PIC device
//           used.
/////////////////////////////////////////////////////////////////////////////
void InitializeBoard(void)
{

    #if defined (PIC24FJ256DA210_DEV_BOARD) && defined(USE_KEYBOARD)

     ANSA = 0x0000;
     ANSB = 0x0020;		// RB5 as potentiometer input
     ANSC = 0x0010;		// RC4 as touch screen X+, RC14 as external source of secondary oscillator
     ANSD = 0x0000;
     ANSE = 0x0000;		// RE9 used as S2
     ANSF = 0x0000;
     ANSG = 0x0080;		// RG8 used as S1, RG7 as touch screen Y+

    #else
        /////////////////////////////////////////////////////////////////////////////
        // ADC Explorer 16 Development Board Errata (work around 2)
        // RB15 should be output
        /////////////////////////////////////////////////////////////////////////////
        #ifndef MEB_BOARD
            LATBbits.LATB15 = 0;
            TRISBbits.TRISB15 = 0;
        #endif
    #endif


        #ifdef MEB_BOARD
            CPLDInitialize();
            CPLDSetGraphicsConfiguration(GRAPHICS_HW_CONFIG);
            CPLDSetSPIFlashConfiguration(SPI_FLASH_CHANNEL);
        #endif // #ifdef MEB_BOARD

    #if defined(__dsPIC33F__) || defined(__PIC24H__) || defined(__dsPIC33E__) || defined(__PIC24E__)

        // Configure Oscillator to operate the device at 40Mhz
        // Fosc= Fin*M/(N1*N2), Fcy=Fosc/2
        #if defined(__dsPIC33E__) || defined(__PIC24E__)
			//Fosc = 8M * 60/(2*2) = 120MHz for 8M input clock
			PLLFBD = 58;    			// M=60
		#else
        	// Fosc= 8M*40(2*2)=80Mhz for 8M input clock
        	PLLFBD = 38;                    // M=40
        #endif
        CLKDIVbits.PLLPOST = 0;         // N1=2
        CLKDIVbits.PLLPRE = 0;          // N2=2
        OSCTUN = 0;                     // Tune FRC oscillator, if FRC is used

        // Disable Watch Dog Timer
        RCONbits.SWDTEN = 0;

        // Clock switching to incorporate PLL
        __builtin_write_OSCCONH(0x03);  // Initiate Clock Switch to Primary

        // Oscillator with PLL (NOSC=0b011)
        __builtin_write_OSCCONL(0x01);  // Start clock switching
        while(OSCCONbits.COSC != 0b011);

        // Wait for Clock switch to occur
        // Wait for PLL to lock
        while(OSCCONbits.LOCK != 1)
        { };

       #if defined(__dsPIC33F__) || defined(__PIC24H__)
        // Set PMD0 pin functionality to digital
        AD1PCFGL = AD1PCFGL | 0x1000;

        #if defined(__dsPIC33FJ128GP804__) || defined(__PIC24HJ128GP504__)
            AD1PCFGLbits.PCFG6 = 1;
            AD1PCFGLbits.PCFG7 = 1;
            AD1PCFGLbits.PCFG8 = 1;
        #endif

        #elif defined(__dsPIC33E__) || defined(__PIC24E__)
            ANSELE = 0x00;
            ANSELDbits.ANSD6 = 0;

		    // Set all touch screen related pins to Analog mode.
	        ANSELBbits.ANSB11 = 1;
        #endif

    #elif defined(__PIC32MX__)
        INTEnableSystemMultiVectoredInt();
        SYSTEMConfigPerformance(GetSystemClock());
    #endif // #if defined(__dsPIC33F__) || defined(__PIC24H__)


    #if defined (EXPLORER_16)
/************************************************************************
* For Explorer 16 RD12 is connected to EEPROM chip select.
* To prevent a conflict between this EEPROM and SST25 flash
* the chip select of the EEPROM SPI should be pulled up.
************************************************************************/
        // Set IOs directions for EEPROM SPI
        MCHP25LC256_CS_LAT = 1;			    // set initial CS value to 1 (not asserted)
    	MCHP25LC256_CS_TRIS = 0;			// set CS pin to output
	#endif // #if defined (EXPLORER_16)

    // Initialize graphics library and create default style scheme for GOL
    GOLInit();

// Set the other chip selects to a known state
#ifdef MIKRO_BOARD
    // SD Card chip select
    LATGbits.LATG9 = 1;
    TRISGbits.TRISG9 = 0;

    // MP3 Codac
    // reset
    LATAbits.LATA5 = 0;
    TRISAbits.TRISA5 = 0;
    // chip select
    LATAbits.LATA2 = 1;
    TRISAbits.TRISA2 = 0;
    // chip select
    LATAbits.LATA3 = 1;
    TRISAbits.TRISA3 = 0;

    AD1PCFGbits.PCFG11 = 1;
    AD1PCFGbits.PCFG10 = 1;
#endif

    //The following are PIC device specific settings for the SPI channel
    //used.

    //Set IOs directions for SST25 SPI
    #if defined (GFX_PICTAIL_V3) || defined (MEB_BOARD) || defined(GFX_PICTAIL_LCC) || defined(MIKRO_BOARD) || defined(GFX_PICTAIL_V3E)

        SST25_CS_LAT = 1;
        SST25_CS_TRIS = 0;

        #ifndef __PIC32MX__
            SST25_SCK_TRIS = 0;
            SST25_SDO_TRIS = 0;
            SST25_SDI_TRIS = 1;
            #if defined(__PIC24FJ256GB210__) || defined(__dsPIC33E__) || defined(__PIC24E__)
            	SST25_SDI_ANS = 0;
    	    #endif
        #endif
    #elif defined (PIC24FJ256DA210_DEV_BOARD)
        SST25_CS_LAT = 1;
        SST25_CS_TRIS = 0;

        // Set the pins to be digital
    	SST25_SDI_ANS = 0;
        SST25_SDO_ANS = 0;

        SST25_SCK_TRIS = 0;
        SST25_SDO_TRIS = 0;
        SST25_SDI_TRIS = 1;

	#endif

    // set the peripheral pin select for the PSI channel used
    #if defined(__dsPIC33FJ128GP804__) || defined(__PIC24HJ128GP504__)
        AD1PCFGL = 0xFFFF;
        RPOR9bits.RP18R = 11;                   // assign RP18 for SCK2
        RPOR8bits.RP16R = 10;                   // assign RP16 for SDO2
        RPINR22bits.SDI2R = 17;                 // assign RP17 for SDI2
    #elif defined(__PIC24FJ256GB110__) || defined(__PIC24FJ256GA110__) || defined (__PIC24FJ256GB210__)
        __builtin_write_OSCCONL(OSCCON & 0xbf); // unlock PPS
        RPOR10bits.RP21R = 11;                  // assign RP21 for SCK2
        RPOR9bits.RP19R = 10;                   // assign RP19 for SDO2
        RPINR22bits.SDI2R = 26;                 // assign RP26 for SDI2
        __builtin_write_OSCCONL(OSCCON | 0x40); // lock   PPS
    #elif defined(__PIC24FJ256DA210__)

        __builtin_write_OSCCONL(OSCCON & 0xbf); // unlock PPS

    	#if (SST25_SPI_CHANNEL == 1)
    	    RPOR1bits.RP2R = 8;                 // assign RP2 for SCK1
    	    RPOR0bits.RP1R = 7;                 // assign RP1 for SDO1
    	    RPINR20bits.SDI1R = 0;              // assign RP0 for SDI1
        #elif (SST25_SPI_CHANNEL == 2)
            RPOR1bits.RP2R = 11;                // assign RP2 for SCK2
    	    RPOR0bits.RP1R = 10;                // assign RP1 for SDO2
    	    RPINR22bits.SDI2R = 0;              // assign RP0 for SDI2
    	#endif

        __builtin_write_OSCCONL(OSCCON | 0x40); // lock   PPS

    #endif

	// initialize the Flash Memory driver
    FlashInit(&SPI_Init_Data);

    // initialize the timer that manages the tick counter
    TickInit();

    // initialize the components for Resistive Touch Screen
    TouchInit(NVMWrite, NVMRead, NVMSectorErase, TOUCH_INIT_VALUES);

   // HardwareButtonInit();    //Graphics       	// Initialize the hardware buttons
        AD1PCFGbits.PCFG15=1;
        TRISBbits.TRISB15=1;

}

WORD Msgsss1(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg)
{
if(pMsg->uiEvent==EVENT_RELEASE)
    screenState=CREATE_sss2;

}
WORD Msgsss2(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg)
{
    XCHAR* ttt="Peter";
    STATICTEXT *src2st;


    switch(GetObjID(pObj))
    {
        case BTN_100:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss1;
                return(1);

                }
        case BTN_101:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss3;
                return(1);
                }
         case STE_28:
              if(objMsg == ST_MSG_SELECTED )
                {
               // StSetText((STATICTEXT *)GOLFindObject(STE_28),((XCHAR*)ttt));
               //  SetState((STATICTEXT *)GOLFindObject(STE_28), ST_UPDATE);
                 return(1);
                }                      
         
                      case BTN_23:
            if(objMsg ==BTN_MSG_RELEASED)
                {
               DmaChnDisable(1);
                CloseTimer5();
                CloseTimer4();
            JumpToApp();   
            }

    }

 return(1);

}

UINT8 start_stop_led = 0;
WORD Msgsss3(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg)
{
  switch(GetObjID(pObj))
    {
        case BTN_30:
            if(objMsg ==BTN_MSG_RELEASED)
                {
      //          screenState = CREATE_sss2;

                start_stop_led = 1;
                return(1);
                }
        case BTN_31:
            if(objMsg ==BTN_MSG_RELEASED)
                {
      //          screenState = CREATE_sss2;
                
                start_stop_led = 0;
                return(1);
                }
        case BTN_41:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss2;
                return(1);

                }
        case BTN_42:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss4;
                return(1);

                }

    }
 return(1);

}
UINT8 time_control = 0;
WORD Msgsss4(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg)
{
  switch(GetObjID(pObj))
    {
        case BTN_43:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss3;
                return(1);

                }
        case BTN_44:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss5;
                return(1);

                }
        case BTN_36:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                time_control = 1;
                return(1);

                }
        case BTN_38:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                time_control = 2;
                return(1);

                }
        case BTN_37:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                time_control = 3;
                return(1);

                }

    }
 return(1);

}

UINT8 round_dial = 0;

WORD Msgsss5(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg)
{
  switch(GetObjID(pObj))
    {
        case RDI_46:
            if(objMsg == RD_MSG_CLOCKWISE)
            {
                 round_dial = 2;
                 return 1;
		// Add code
            }else if(objMsg == RD_MSG_CTR_CLOCKWISE)
            {
                 round_dial = 1;
                 return 1;
		// Add code
            }
        case BTN_62:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss4;
                return(1);

                }
        case BTN_63:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss6;
                return(1);                
                }
      case SLD_47:
          if(objMsg == OBJ_MSG_PASSIVE)
          {

          }



  }

}
WORD Msgsss6(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg)
{
   switch(GetObjID(pObj))
    {
        case BTN_92:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss5;
                usbState=no_check_usb;
                return(1);

                }
        case BTN_93:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss7;
               usbState=no_check_usb;
                return(1);

                }
            case BTN_87:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                usbState=no_check_usb;
                screenState = CREATE_ACCL;
                return(1);
                }
       case BTN_91:
           if(objMsg ==BTN_MSG_RELEASED)
                {
                usbState = write_usb;
                return(1);
                }
           case BTN_90:
           if(objMsg ==BTN_MSG_RELEASED)
                {
                usbState = read_usb;
                return(1);
                }
    }

}
WORD Msgsss7(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg)
{

 switch(GetObjID(pObj))
    {
        case BTN_97:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss6;
                return(1);

                }
        case BTN_94:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss8;
                usbState = check_usb;
                return(1);

                }
   }
return(1);
}
WORD Msgsss8(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg)
{
    XCHAR* ttt="Image Finded,Downloading!";
    STATICTEXT *src2st;
volatile UINT i;
    switch(GetObjID(pObj))
    {
        case BTN_98:
            if(objMsg ==BTN_MSG_RELEASED)
                {
                screenState = CREATE_sss7;
                return(1);

                }
     }
    if(usbState == usb_attach)
    {
        StSetText((STATICTEXT *)GOLFindObject(STE_95),((XCHAR*)ttt));
                 SetState((STATICTEXT *)GOLFindObject(STE_95), ST_UPDATE);
    
    }
                 return(1);

}


//Graphics program end
//ACCL start
        /* Create the page*/
void CreatePage(XCHAR *pText)
{
    OBJ_HEADER  *obj;
    SHORT       i;

    WndCreate
    (
        ID_WINDOW1,                 // ID
        0,
        0,
        GetMaxX(),
        GetMaxY(),                  // dimension
        0x6000,                   // will be dislayed after creation
        NULL,         // icon
        pText,                      // set text
        navScheme
    );                              // default GOL scheme


}


/* */




// dimensions for Accelerometer graph area
#define ACCL_ORIGIN_X        ((GetMaxX() - 260 + 1) / 2)
#define ACCL_ORIGIN_Y        ((40 + GetMaxY() - 185 + 1) / 2)
#define ACCL_PANEL_LEFT      ACCL_ORIGIN_X
#define ACCL_PANEL_RIGHT     ACCL_ORIGIN_X + 260
#define ACCL_PANEL_TOP       ACCL_ORIGIN_Y
#define ACCL_PANEL_BOTTOM    ACCL_ORIGIN_Y + 160

// Graph area borders
#define ACCLGR_LEFT      (ACCL_PANEL_LEFT + GOL_EMBOSS_SIZE)
#define ACCLGR_RIGHT     (ACCL_PANEL_RIGHT - GOL_EMBOSS_SIZE)
#define ACCLGR_TOP       (ACCL_PANEL_TOP + GOL_EMBOSS_SIZE)
#define ACCLGR_BOTTOM    (ACCL_PANEL_BOTTOM - GOL_EMBOSS_SIZE)

volatile SHORT  buffer[256];
volatile SHORT  counter = 0;
volatile SHORT  inc = 10;

// Creates Accelerometer screen
void CreateAccelerometer(void)
{
    GOLFree();  // free memory for the objects in the previous linked list and start new list
    CreatePage((XCHAR *)AcclDemoStr);
   
}

// Draws box for Accelerometer graph
WORD PanelAccelerometer(void)
{
    #define ACCLPNL_STATE_SET    0
    #define ACCLPNL_STATE_DRAW   1
    #define ACCLPNL_STATE_TEXT   2

    static BYTE state = ACCLPNL_STATE_SET;

    if(state == ACCLPNL_STATE_SET)
    {                               // set data for panel
        GOLPanelDraw
        (
            ACCL_PANEL_LEFT,
            ACCL_PANEL_TOP,
            ACCL_PANEL_RIGHT,
            ACCL_PANEL_BOTTOM,
            0,
            WHITE,
            altScheme->EmbossDkColor,
            altScheme->EmbossLtColor,
            NULL,
            GOL_EMBOSS_SIZE
        );

        state = ACCLPNL_STATE_DRAW;  // change state
    }

    if(state == ACCLPNL_STATE_DRAW)
    {
        if(!GOLPanelDrawTsk())
        {                           // draw box for graph
            return (0);             // drawing is not completed
        }
        else
        {
            state = ACCLPNL_STATE_TEXT;          // change to initial state
            SetFont((void *)&Gentium_16);
            SetColor(BRIGHTBLUE);
        }
    }

    if(state == ACCLPNL_STATE_TEXT)
    {
        state = ACCLPNL_STATE_SET;               // change to initial state
        return (1);                             // drawing is done
    }

    return (1);
}

// Add sample from Accelerometer into temporary buffer

WORD GetACCLSamples( WORD number)
{
    static BYTE     counter = 0;
    volatile SHORT  temp_x, temp_y, temp_z;

	ACCEL_DATA raw_x, raw_y, raw_z;

#ifdef ACCEL_USE_POLLING
        ACCELTask();
#endif

    if(ACCELGetXYZAxis(&raw_x, &raw_y, &raw_z) == ACCEL_VALID)
    {
		temp_x = raw_x;
		temp_y = raw_y;
		temp_z = raw_z;
    	if((temp_x + ACCLGR_TOP) > ACCLGR_BOTTOM)
        	temp_x = ACCLGR_BOTTOM - ACCLGR_TOP;

	    tempBuffer_X[counter] = temp_x;

    	if((temp_y + ACCLGR_TOP) > ACCLGR_BOTTOM)
        	temp_y = ACCLGR_BOTTOM - ACCLGR_TOP;

	    tempBuffer_Y[counter] = temp_y;

    	if((temp_z + ACCLGR_TOP) > ACCLGR_BOTTOM)
        	temp_z = ACCLGR_BOTTOM - ACCLGR_TOP;

	    tempBuffer_Z[counter] = temp_z;
		counter++;
	    if(counter >= number)
	    {
	       	counter = 0;
	       	return (1);
	    }
	}
    return (0);
}

#ifdef ACCEL_USE_EXTERNAL_INTERUPT_HANDLER
__ISR(_I2C_2_VECTOR, ipl2) I2C2InterruptHandler(void)
{
    if(ACCELTask() == ACCEL_INVALID)
    {
        INTClearFlag(INT_I2C2M);
        INTEnable(INT_I2C2M, INT_DISABLED);
    }
}
#endif
// Draws graph

void GraphAccelerometer(void)
{
    SHORT           x, y;
    SHORT           sy, ey;
    SHORT           *ptr_x;
	SHORT           *ptr_y;
    SHORT           *ptr_z;
	SHORT           counter;
    static SHORT    pos_x;
    static SHORT    pos_y;
    static SHORT    pos_z;

    // Plotting X axis
	if (Axes == X ||Axes == XY ||Axes == XZ ||Axes == XYZ )
	{
	    // remove graph
	    SetColor(WHITE);
	    ptr_x = acclBuffer_X + pos_x;
	    sy = *ptr_x++;
	    for(x = ACCLGR_RIGHT; x >= ACCLGR_LEFT; x--)
	    {
	        if(ptr_x == (acclBuffer_X + ACCL_BUFFER_SIZE))
	            ptr_x = acclBuffer_X;
	        ey = *ptr_x++;
	        if(ey > sy)
	        {
	            for(y = sy + ACCLGR_TOP; y < ey + ACCLGR_TOP + 1; y++)
	            {
					switch(Accl_Sensivity_Select)// Y co-ordinate is calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
	        }
	        else
	        {
	            for(y = ey + ACCLGR_TOP; y < sy + ACCLGR_TOP + 1; y++)
	            {
					switch(Accl_Sensivity_Select)// Y co-ordinate is calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
	        	}
			}

	        sy = ey;
	    }
		// draw grid
    	SetColor(LIGHTGRAY);
    	for(x = ACCLGR_LEFT + ((ACCLGR_RIGHT - ACCLGR_LEFT) >> 3); x < ACCLGR_RIGHT; x += (ACCLGR_RIGHT - ACCLGR_LEFT) >> 3)
        	WAIT_UNTIL_FINISH(Bar(x, ACCLGR_TOP, x, ACCLGR_BOTTOM));

    	for(y = ACCLGR_TOP + ((ACCLGR_BOTTOM - ACCLGR_TOP) >> 3); y < ACCLGR_BOTTOM; y += (ACCLGR_BOTTOM - ACCLGR_TOP) >> 3)
    	    WAIT_UNTIL_FINISH(Bar(ACCLGR_LEFT, y, ACCLGR_RIGHT, y));

    	pos_x -= ACCL_MOVE_DELTA;
    	if(pos_x < 0)
    	    pos_x = ACCL_BUFFER_SIZE - 1;

    	// copy new data from temporary buffer
	    ptr_x = acclBuffer_X + pos_x;
	    for(counter = ACCL_MOVE_DELTA - 1; counter >= 0; counter--)
	    {
	        *ptr_x++ = tempBuffer_X[counter];
	        if(ptr_x == (acclBuffer_X + ACCL_BUFFER_SIZE))
	            ptr_x = acclBuffer_X;
	    }

	    // draw graph
	    SetColor(BRIGHTRED);
	    ptr_x = acclBuffer_X + pos_x;
	    sy = *ptr_x++;
	    for(x = ACCLGR_RIGHT; x >= ACCLGR_LEFT; x--)
	    {
	        if(ptr_x == (acclBuffer_X + ACCL_BUFFER_SIZE))
	            ptr_x = acclBuffer_X;
	        ey = *ptr_x++;
	        if(ey > sy)
	        {
	            for(y = sy + ACCLGR_TOP; y < ey + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select) // Y is co-ordinate calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
	        }
	        else
	        {
	            for(y = ey + ACCLGR_TOP; y < sy + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select)// Y is co-ordinate calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
	        }

	        sy = ey;
	    }

	}

	// Plotting Y Axis
	if (Axes == Y ||Axes == XY ||Axes == YZ ||Axes == XYZ )
	{
		// remove graph
		SetColor(WHITE);
		ptr_y = acclBuffer_Y + pos_y;
		sy = *ptr_y++;
		for(x = ACCLGR_RIGHT; x >= ACCLGR_LEFT; x--)
		{
		    if(ptr_y == (acclBuffer_Y + ACCL_BUFFER_SIZE))
		        ptr_y = acclBuffer_Y;
		    ey = *ptr_y++;
		    if(ey > sy)
		    {
		        for(y = sy + ACCLGR_TOP; y < ey + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select)// Y is co-ordinate calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
		    }
		    else
		    {
		        for(y = ey + ACCLGR_TOP; y < sy + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select)// Y co-ordinate is calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
		    }

		    sy = ey;
		}
	    // draw grid
    	SetColor(LIGHTGRAY);
    	for(x = ACCLGR_LEFT + ((ACCLGR_RIGHT - ACCLGR_LEFT) >> 3); x < ACCLGR_RIGHT; x += (ACCLGR_RIGHT - ACCLGR_LEFT) >> 3)
        	WAIT_UNTIL_FINISH(Bar(x, ACCLGR_TOP, x, ACCLGR_BOTTOM));

	    for(y = ACCLGR_TOP + ((ACCLGR_BOTTOM - ACCLGR_TOP) >> 3); y < ACCLGR_BOTTOM; y += (ACCLGR_BOTTOM - ACCLGR_TOP) >> 3)
	        WAIT_UNTIL_FINISH(Bar(ACCLGR_LEFT, y, ACCLGR_RIGHT, y));

	    pos_y -= ACCL_MOVE_DELTA;
	    if(pos_y < 0)
	        pos_y = ACCL_BUFFER_SIZE - 1;

	    // copy new data from temporary buffer
	    ptr_y = acclBuffer_Y + pos_y;
	    for(counter = ACCL_MOVE_DELTA - 1; counter >= 0; counter--)
	    {
	        *ptr_y++ = tempBuffer_Y[counter];
	        if(ptr_y == (acclBuffer_Y + ACCL_BUFFER_SIZE))
	            ptr_y = acclBuffer_Y;
	    }

	    // draw graph
	    SetColor(GREEN);
	    ptr_y = acclBuffer_Y + pos_y;
	    sy = *ptr_y++;
	    for(x = ACCLGR_RIGHT; x >= ACCLGR_LEFT; x--)
	    {
	        if(ptr_y == (acclBuffer_Y + ACCL_BUFFER_SIZE))
	            ptr_y = acclBuffer_Y;
	        ey = *ptr_y++;
	        if(ey > sy)
	        {
	            for(y = sy + ACCLGR_TOP; y < ey + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select)// Y co-ordinate is calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
	        }
	        else
	        {
	            for(y = ey + ACCLGR_TOP; y < sy + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select)// Y co-ordinate is calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
	        }

	        sy = ey;
	    }

	}
	//Plotting Z Axis
	if (Axes == Z ||Axes == YZ ||Axes == XZ ||Axes == XYZ )
	{
		// remove graph
		 SetColor(WHITE);
		 ptr_z = acclBuffer_Z + pos_z;
		 sy = *ptr_z++;
		 for(x = ACCLGR_RIGHT; x >= ACCLGR_LEFT; x--)
		 {
		     if(ptr_z == (acclBuffer_Z + ACCL_BUFFER_SIZE))
		         ptr_z = acclBuffer_Z;
		     ey = *ptr_z++;
		     if(ey > sy)
		     {
		         for(y = sy + ACCLGR_TOP; y < ey + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select)
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
		     }
		     else
		     {
		         for(y = ey + ACCLGR_TOP; y < sy + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select)// Y co-ordinate is calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
		     }

		     sy = ey;
		 }
	    // draw grid
    	SetColor(LIGHTGRAY);
	    for(x = ACCLGR_LEFT + ((ACCLGR_RIGHT - ACCLGR_LEFT) >> 3); x < ACCLGR_RIGHT; x += (ACCLGR_RIGHT - ACCLGR_LEFT) >> 3)
	        WAIT_UNTIL_FINISH(Bar(x, ACCLGR_TOP, x, ACCLGR_BOTTOM));

	    for(y = ACCLGR_TOP + ((ACCLGR_BOTTOM - ACCLGR_TOP) >> 3); y < ACCLGR_BOTTOM; y += (ACCLGR_BOTTOM - ACCLGR_TOP) >> 3)
	        WAIT_UNTIL_FINISH(Bar(ACCLGR_LEFT, y, ACCLGR_RIGHT, y));

	    pos_z -= ACCL_MOVE_DELTA;
	    if(pos_z < 0)
	        pos_z = ACCL_BUFFER_SIZE - 1;

	    // copy new data from temporary buffer
	    ptr_z = acclBuffer_Z + pos_z;
	    for(counter = ACCL_MOVE_DELTA - 1; counter >= 0; counter--)
	    {
	        *ptr_z++ = tempBuffer_Z[counter];
	        if(ptr_z == (acclBuffer_Z + ACCL_BUFFER_SIZE))
	            ptr_z = acclBuffer_Z;
	    }

	    // draw graph
	    SetColor(BRIGHTBLUE);
	    ptr_z = acclBuffer_Z + pos_z;
	    sy = *ptr_z++;
	    for(x = ACCLGR_RIGHT; x >= ACCLGR_LEFT; x--)
	    {
	        if(ptr_z == (acclBuffer_Z + ACCL_BUFFER_SIZE))
	            ptr_z = acclBuffer_Z;
	        ey = *ptr_z++;
	        if(ey > sy)
	        {
	            for(y = sy + ACCLGR_TOP; y < ey + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select)// Y co-ordinate is calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
	        }
	        else
	        {
	            for(y = ey + ACCLGR_TOP; y < sy + ACCLGR_TOP + 1; y++)
				{
	               	switch(Accl_Sensivity_Select)// Y co-ordinate is calculated and plotted as per the accelerometer sensitivity selection
					{
						case ACCEL_BMA150_RANGE_2G:
							PutPixel(x, (y/8)+120);
							break;
						case ACCEL_BMA150_RANGE_4G:
							PutPixel(x, (y/4)+110);
							break;
						case ACCEL_BMA150_RANGE_8G:
							PutPixel(x, (y/2)+100);
							break;
						default:
							break;
					}
				}
	        }

	        sy = ey;
	    }
	}
}

// Processes messages for ECG screen
WORD MsgAccelerometer(WORD objMsg, OBJ_HEADER *pObj,GOL_MSG *pMsg)
{
    if(pMsg->uiEvent==EVENT_RELEASE)
    screenState=CREATE_sss6;
}


// Output text message on screen and stop execution
void ErrorTrap(XCHAR *message)
{
    SetColor(BLACK);
    ClearDevice();
    SetFont((void *) &FONTDEFAULT);
    SetColor(WHITE);
    while(!OutTextXY(0, 0, message));
    while(1);
}

//ACCL end

void sss6Drawcallback(void)
{
   /*
    if((tick - prevTick) >500)
    {
        while (ACCELGetTemperature(&temperature)== ACCEL_INVALID);
        //sprintf((char *)temperature_str, "Temp: %dC, x = %d, y = %d, z = %d", temperature,accel_x,accel_y,accel_z);  // Convert the temperature readings into a string to display
        #ifdef ACCEL_USE_POLLING
        ACCELTask();
        #endif
        if(ACCELGetXYZAxis(&accel_x, &accel_y, &accel_z) == ACCEL_VALID){};
        prevTick = tick;
    
        sprintf((char *)temperature_str, "%d         %d         %d         %dC", accel_x,accel_y,accel_z,temperature);  // Convert the temperature readings into a string to display
                 StSetText((STATICTEXT *)GOLFindObject(STE_72),((XCHAR*)temperature_str));
                 SetState((STATICTEXT *)GOLFindObject(STE_72), ST_UPDATE);
      }
*/

}
void sss2Drawcallback(void)
{
    static DWORD prevTick = 0;
    if(tick - prevTick > 500){
        prevTick = tick;
    rtcc_to_string();
    StSetText((STATICTEXT *)GOLFindObject(STE_28),(XCHAR *)rtcc_str);
    SetState((STATICTEXT *)GOLFindObject(STE_28),ST_DRAW);
    }
}
void sss3Drawcallback(void)
{

    static UINT8 key_entry = 0;
    static UINT8 start = 0;
    static UINT8 state = 0;
    static DWORD prevTick = 0;
    static UINT8 step = 0;
    
    step = SldGetPos((SLIDER *)GOLFindObject(SLD_29));

    if(key_entry == 0){
        if(CheckTrigger()){
            key_entry = 1;
            if(start == 0){
                start = 1;
            }else start = 0;
        }
    }

    if(start_stop_led == 1){
        start_stop_led = 2;
            start = 1;
    }
    if(start_stop_led == 0){
        start_stop_led = 2;
            start = 0;
    }

    if(!CheckTrigger()){
            key_entry = 0;
    }
    
    if(start == 1){
        switch(state){
            case 0:
                LED1_OFF();LED2_OFF();LED3_OFF();;break;
            case 1:
                LED1_ON();LED2_OFF();LED3_OFF();break;
            case 2:
                LED1_OFF();LED2_ON();LED3_OFF();break;
            case 3:
                LED1_OFF();LED2_OFF();LED3_ON();break;
//            case 4:
 //               LED1_OFF();LED2_OFF();LED3_OFF();LED4_ON();LED5_OFF();break;
 //           case 5:
 //               LED1_OFF();LED2_OFF();LED3_OFF();LED4_OFF();LED5_ON();break;
            default:break;
        }
        if(tick > (prevTick+step)){
            state++;
            if(state > 3){
                state = 0;
            }
            prevTick = tick;
        }
    }
    
}
void sss4Drawcallback(void)
{
    static DWORD prevTick = 0;
    if(tick - prevTick > 500){
        prevTick = tick;
    rtcc_to_string();
    StSetText((STATICTEXT *)GOLFindObject(STE_32),(XCHAR *)rtcc_str);
    SetState((STATICTEXT *)GOLFindObject(STE_32),ST_DRAW);

    if(time_control == 1){
        time_control = 0;
        OpenTimer5(T5_ON | T5_PS_1_256, T5_TICK);
        ConfigIntTimer5(T5_INT_ON | T5_INT_PRIOR_3);
    }else if(time_control == 3){
        time_control = 0;
        CloseTimer5();
        rtcc.time.hour = 0;
        rtcc.time.minutes = 0;
        rtcc.time.seconds = 0;
        rtcc.date.year = 0;
        rtcc.date.month = 0;
        rtcc.date.day = 0;
    }else if(time_control == 2){
        time_control = 0;
        CloseTimer5();
    }
    }

}

UINT16 time_str[3];
UINT16 time_str1[3];
UINT16 time_str2[3];
UINT16 time_str3[3];
UINT16 time_str4[3];
UINT16 time_str5[3];
UINT16 time_str6[3];
void sss5Drawcallback(void)
{
    
    unsigned char my_slider_pos = 0;
    static DWORD prevTick = 0;
    static DWORD flashTick = 0;
    static UINT8 state = 0;
    set_rtcc.date.year = rtcc.date.year;
    set_rtcc.date.month = rtcc.date.month;
    set_rtcc.date.day = rtcc.date.day;
    set_rtcc.time.hour = rtcc.time.hour;
    set_rtcc.time.minutes = rtcc.time.minutes;
    set_rtcc.time.seconds = rtcc.time.seconds;
    if(tick - prevTick > 100){
        prevTick = tick;
        sprintf((char *)time_str1, "%d",set_rtcc.date.year);
        StSetText((STATICTEXT *)GOLFindObject(STE_61),(XCHAR *)time_str1);
        SetState((STATICTEXT *)GOLFindObject(STE_61),ST_DRAW);


        sprintf((char *)time_str2, "%d",set_rtcc.date.month);
        StSetText((STATICTEXT *)GOLFindObject(STE_64),(XCHAR *)time_str2);
        SetState((STATICTEXT *)GOLFindObject(STE_64),ST_DRAW);


        sprintf((char *)time_str3, "%d",set_rtcc.date.day);
        StSetText((STATICTEXT *)GOLFindObject(STE_65),(XCHAR *)time_str3);
        SetState((STATICTEXT *)GOLFindObject(STE_65),ST_DRAW);

        sprintf((char *)time_str4, "%d",set_rtcc.time.hour);
        StSetText((STATICTEXT *)GOLFindObject(STE_66),(XCHAR *)time_str4);
        SetState((STATICTEXT *)GOLFindObject(STE_66),ST_DRAW);

        sprintf((char *)time_str5, "%d",set_rtcc.time.minutes);
        StSetText((STATICTEXT *)GOLFindObject(STE_67),(XCHAR *)time_str5);
        SetState((STATICTEXT *)GOLFindObject(STE_67),ST_DRAW);

        sprintf((char *)time_str6, "%d",set_rtcc.time.seconds);
        StSetText((STATICTEXT *)GOLFindObject(STE_68),(XCHAR *)time_str6);
        SetState((STATICTEXT *)GOLFindObject(STE_68),ST_DRAW);

        my_slider_pos = SldGetPos((SLIDER *)GOLFindObject(SLD_47));

        if((0 <= my_slider_pos)&&(my_slider_pos <= 16)){
     //       GOLSetFocus((STATICTEXT *)GOLFindObject(STE_61));
            if(tick > (flashTick + 500)){
                flashTick = tick;
                if(state == 0){
                    sprintf((char *)time_str1, "%d",set_rtcc.date.year);
                    StSetText((STATICTEXT *)GOLFindObject(STE_61),(XCHAR *)time_str1);
                    SetState((STATICTEXT *)GOLFindObject(STE_61),ST_DRAW);
                }else{
                    time_str1[0] = ' ';time_str1[1] = ' ';time_str1[2] = ' ';
                    StSetText((STATICTEXT *)GOLFindObject(STE_61),(XCHAR *)time_str1);
                    SetState((STATICTEXT *)GOLFindObject(STE_61),ST_DRAW);
                }
                if(state == 0)state = 1;
                else state = 0;
            }
            if(round_dial == 1){
                round_dial = 0;
                if(set_rtcc.date.year < 13)
                    set_rtcc.date.year++;
            }else if(round_dial == 2){
                round_dial = 0;
                if(set_rtcc.date.year > 0)
                    set_rtcc.date.year--;
            }
        }else if((17 <= my_slider_pos)&&(my_slider_pos <= 33)){


            //GOLSetFocus((STATICTEXT *)GOLFindObject(STE_64));

            if(tick > (flashTick + 500)){
                flashTick = tick;
                if(state == 0){
                    sprintf((char *)time_str2, "%d",set_rtcc.date.month);
                    StSetText((STATICTEXT *)GOLFindObject(STE_64),(XCHAR *)time_str2);
                    SetState((STATICTEXT *)GOLFindObject(STE_64),ST_DRAW);
                }else{
                    time_str2[0] = ' ';time_str2[1] = ' ';time_str2[2] = ' ';
                    StSetText((STATICTEXT *)GOLFindObject(STE_64),(XCHAR *)time_str2);
                    SetState((STATICTEXT *)GOLFindObject(STE_64),ST_DRAW);
                }
                if(state == 0)state = 1;
                else state = 0;
            }
            if(round_dial == 1){
                round_dial = 0;
                if(set_rtcc.date.month < 12)
                    set_rtcc.date.month++;
            }else if(round_dial == 2){
                round_dial = 0;
                if(set_rtcc.date.month > 1)
                    set_rtcc.date.month--;
            }
        }else if((34 <= my_slider_pos)&&(my_slider_pos <= 50)){
        //    GOLSetFocus((STATICTEXT *)GOLFindObject(STE_65));
            if(tick > (flashTick + 500)){
                flashTick = tick;
                if(state == 0){
                    sprintf((char *)time_str3, "%d",set_rtcc.date.day);
                    StSetText((STATICTEXT *)GOLFindObject(STE_65),(XCHAR *)time_str3);
                    SetState((STATICTEXT *)GOLFindObject(STE_65),ST_DRAW);
                }else{
                    time_str3[0] = ' ';time_str3[1] = ' ';time_str3[2] = ' ';
                    StSetText((STATICTEXT *)GOLFindObject(STE_65),(XCHAR *)time_str3);
                    SetState((STATICTEXT *)GOLFindObject(STE_65),ST_DRAW);
                }
                if(state == 0)state = 1;
                else state = 0;
            }
            if(round_dial == 1){
                round_dial = 0;
                switch(set_rtcc.date.month){
                    case 1:
                        if(set_rtcc.date.day < 31)  set_rtcc.date.day++;break;
                    case 2:
                        if(!(set_rtcc.date.year%4)){
                            if(set_rtcc.date.day < 29)  set_rtcc.date.day++;break;
                        }else{
                            if(set_rtcc.date.day < 28)  set_rtcc.date.day++;break;
                        }

                    case 3:
                        if(set_rtcc.date.day < 31)  set_rtcc.date.day++;break;
                    case 4:
                        if(set_rtcc.date.day < 30)  set_rtcc.date.day++;break;
                    case 5:
                        if(set_rtcc.date.day < 31)  set_rtcc.date.day++;break;
                    case 6:
                        if(set_rtcc.date.day < 30)  set_rtcc.date.day++;break;
                    case 7:
                        if(set_rtcc.date.day < 31)  set_rtcc.date.day++;break;
                    case 8:
                        if(set_rtcc.date.day < 31)  set_rtcc.date.day++;break;
                    case 9:
                        if(set_rtcc.date.day < 30)  set_rtcc.date.day++;break;
                    case 10:
                        if(set_rtcc.date.day < 31)  set_rtcc.date.day++;break;
                    case 11:
                        if(set_rtcc.date.day < 30)  set_rtcc.date.day++;break;
                    case 12:
                        if(set_rtcc.date.day < 31)  set_rtcc.date.day++;break;
                    default: break;
                }

            }else if(round_dial == 2){
                round_dial = 0;
                if(set_rtcc.date.day > 1)
                    set_rtcc.date.day--;
            }
        }else if((51 <= my_slider_pos)&&(my_slider_pos <= 67)){
            if(round_dial == 1){
                round_dial = 0;
                if(set_rtcc.time.hour < 24)
                    set_rtcc.time.hour++;
            }else if(round_dial == 2){
                round_dial = 0;
                if(set_rtcc.time.hour > 0)
                    set_rtcc.time.hour--;
            }
      //      GOLSetFocus((STATICTEXT *)GOLFindObject(STE_66));
               if(tick > (flashTick + 500)){
                flashTick = tick;
                if(state == 0){
                    sprintf((char *)time_str4, "%d",set_rtcc.time.hour);
                    StSetText((STATICTEXT *)GOLFindObject(STE_66),(XCHAR *)time_str4);
                    SetState((STATICTEXT *)GOLFindObject(STE_66),ST_DRAW);
                }else{
                    time_str4[0] = ' ';time_str4[1] = ' ';time_str4[2] = ' ';
                    StSetText((STATICTEXT *)GOLFindObject(STE_66),(XCHAR *)time_str4);
                    SetState((STATICTEXT *)GOLFindObject(STE_66),ST_DRAW);
                }
                if(state == 0)state = 1;
                else state = 0;
            }
        }else if((68 <= my_slider_pos)&&(my_slider_pos <= 84)){
            if(round_dial == 1){
                round_dial = 0;
                if(set_rtcc.time.minutes < 59)
                    set_rtcc.time.minutes++;
            }else if(round_dial == 2){
                round_dial = 0;
                if(set_rtcc.time.minutes > 0)
                    set_rtcc.time.minutes--;
            }

       //     GOLSetFocus((STATICTEXT *)GOLFindObject(STE_67));
                if(tick > (flashTick + 500)){
                flashTick = tick;
                if(state == 0){
                    sprintf((char *)time_str5, "%d",set_rtcc.time.minutes);
                    StSetText((STATICTEXT *)GOLFindObject(STE_67),(XCHAR *)time_str5);
                    SetState((STATICTEXT *)GOLFindObject(STE_67),ST_DRAW);
                }else{
                    time_str5[0] = ' ';time_str5[1] = ' ';time_str5[2] = ' ';
                    StSetText((STATICTEXT *)GOLFindObject(STE_67),(XCHAR *)time_str5);
                    SetState((STATICTEXT *)GOLFindObject(STE_67),ST_DRAW);
                }
                if(state == 0)state = 1;
                else state = 0;
            }
        }else if((85 <= my_slider_pos)&&(my_slider_pos <= 100)){
            if(round_dial == 1){
                round_dial = 0;
                if(set_rtcc.time.seconds < 59)
                    set_rtcc.time.seconds++;
            }else if(round_dial == 2){
                round_dial = 0;
                if(set_rtcc.time.seconds > 0)
                    set_rtcc.time.seconds--;
            }

        //    GOLSetFocus((STATICTEXT *)GOLFindObject(STE_68));
        if(tick > (flashTick + 500)){
                flashTick = tick;
                if(state == 0){
                    sprintf((char *)time_str6, "%d",set_rtcc.time.seconds);
                    StSetText((STATICTEXT *)GOLFindObject(STE_68),(XCHAR *)time_str6);
                    SetState((STATICTEXT *)GOLFindObject(STE_68),ST_DRAW);
                }else{
                    time_str6[0] = ' ';time_str6[1] = ' ';time_str6[2] = ' ';
                    StSetText((STATICTEXT *)GOLFindObject(STE_68),(XCHAR *)time_str6);
                    SetState((STATICTEXT *)GOLFindObject(STE_68),ST_DRAW);
                }
                if(state == 0)state = 1;
                else state = 0;
            }
        }
        rtcc.date.year = set_rtcc.date.year;
        rtcc.date.month = set_rtcc.date.month;
        rtcc.date.day = set_rtcc.date.day;
        rtcc.time.hour = set_rtcc.time.hour;
        rtcc.time.minutes = set_rtcc.time.minutes;
        rtcc.time.seconds = set_rtcc.time.seconds;

        rtcc_to_string();
        StSetText((STATICTEXT *)GOLFindObject(STE_45),(XCHAR *)rtcc_str);
        SetState((STATICTEXT *)GOLFindObject(STE_45),ST_DRAW);
    }
}
    // Setup configuration

void __ISR(_TIMER_4_VECTOR, ipl1) _T4Interrupt(void)
{

    mT4ClearIntFlag();

}
