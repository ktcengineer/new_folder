/* 
 * File:   soft_rtcc.h
 * Author: Administrator
 *
 * Created on 2013年7月26日, 上午6:01
 */

#ifndef SOFT_RTCC_H
#define	SOFT_RTCC_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef struct
{
	struct
	{
		unsigned char seconds;
		unsigned char minutes;
		unsigned char hour;
	}time;
	struct
	{
		unsigned char day;
		unsigned char dayofweek;
		unsigned char month;
		unsigned char year;
 	}date;
}  DATETIME;

// DATETIME structure which contains the time and date
// see main.h
DATETIME rtcc;
DATETIME alarm;
DATETIME set_rtcc;

unsigned char DayOfWeek ( DATETIME getDayOfWeek );
void soft_rtcc_init(void);
void rtcc_to_string(void);
UINT16 rtcc_str[30];
#ifdef	__cplusplus
}
#endif

#endif	/* SOFT_RTCC_H */

