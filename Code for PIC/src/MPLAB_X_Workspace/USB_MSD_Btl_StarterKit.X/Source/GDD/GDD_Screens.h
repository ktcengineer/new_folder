
/*****************************************************************************
* Microchip Graphics Library
* Graphics Display Designer (GDD) Template
*****************************************************************************
 
* Dependencies:    See INCLUDES section below
* Processor:       PIC24F, PIC24H, dsPIC, PIC32
* Compiler:        MPLAB C30 V3.22, MPLAB C32 V1.05b
* Linker:          MPLAB LINK30, MPLAB LINK32
* Company:         Microchip Technology Incorporated
*
* Software License Agreement
*
* Copyright (c) 2010 Microchip Technology Inc.  All rights reserved.
* Microchip licenses to you the right to use, modify, copy and distribute
* Software only when embedded on a Microchip microcontroller or digital
* signal controller, which is integrated into your product or third party
* product (pursuant to the sublicense terms in the accompanying license
* agreement).  
*
* You should refer to the license agreement accompanying this Software
* for additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
* OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
* PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
* OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 
* BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
* DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
* INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
* COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
* CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
* OR OTHER SIMILAR COSTS.
*
* Author               Date        Comment
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*****************************************************************************/


#ifndef    _GDD_SCREENS_H_
#define    _GDD_SCREENS_H_


/***************************************************
*FUNCTION DECLARATION
***************************************************/

void GDDDemoCreateFirstScreen(void);
void GDDDemoGOLDrawCallback(void);
void GDDDemoNextScreen(void);
void GDDDemoGoToScreen(int screenIndex);
void GDDDemoGOLMsgCallback(WORD objMsg, OBJ_HEADER *pObj, GOL_MSG *pMsg);


/***************************************************
*IMAGE DECLARATION
***************************************************/
extern const IMAGE_FLASH BLUE_NEXT4040;
extern const IMAGE_FLASH BLUE_PREVIOUS4040;
extern const IMAGE_FLASH img_default;


/***************************************************
*FONT DECLARATION
***************************************************/
extern const FONT_FLASH Arial_20;
extern const FONT_FLASH Aharoni_Bold_24;
extern const FONT_FLASH Monospaced_plain_38;
extern const FONT_FLASH Gentium_16;


/***************************************************
*SCREEN DECLARATION
***************************************************/
void Createsrc1(void);
void CreatePrimitivesForsrc1(void);
void Createsrc2(void);
void CreatePrimitivesForsrc2(void);
void Createsrc3(void);
void Createsrc4(void);
void Createsrc5(void);
void CreatePrimitivesForsrc5(void);
void Createsrc6(void);
void CreatePrimitivesForsrc6(void);
void Createsrc7(void);
void Createsrc8(void);
void CreateGraphic(void);



/***************************************************
*UNIQUE WIDGET ID'S
***************************************************/
#define PCB_15 1
#define STE_18 2
#define BTN_23 3
#define BTN_25 4
#define STE_28 5
#define BTN_100 6
#define BTN_101 7
#define SLD_29 8
#define BTN_30 9
#define BTN_31 10
#define BTN_41 11
#define BTN_42 12
#define STE_32 13
#define BTN_36 14
#define BTN_37 15
#define BTN_38 16
#define BTN_43 17
#define BTN_44 18
#define STE_45 19
#define RDI_46 20
#define SLD_47 21
#define BTN_48 22
#define STE_61 23
#define BTN_62 24
#define BTN_63 25
#define STE_64 26
#define STE_65 27
#define STE_66 28
#define STE_67 29
#define STE_68 30
#define STE_72 31
#define STE_76 32
#define STE_77 33
#define STE_78 34
#define BTN_87 35
#define BTN_90 36
#define BTN_91 37
#define BTN_92 38
#define BTN_93 39
#define BTN_94 40
#define BTN_97 41
#define STE_95 42
#define BTN_98 43
#define PRB_73 44

#define	NUM_GDD_SCREENS 9
#endif
