#ifndef _GDD_GRAPHICSCONFIG_H_
#define _GDD_GRAPHICSCONFIG_H_


#undef COLOR_DEPTH
#define COLOR_DEPTH 16

#ifndef USE_BUTTON
#define USE_BUTTON
#endif

#ifndef USE_SLIDER
#define USE_SLIDER
#endif

#ifndef USE_PROGRESSBAR
#define USE_PROGRESSBAR
#endif

#ifndef USE_STATICTEXT
#define USE_STATICTEXT
#endif

#ifndef USE_PICTURE
#define USE_PICTURE
#endif

#ifndef USE_ROUNDDIAL
#define USE_ROUNDDIAL
#endif


#ifndef USE_FONT_FLASH
#define USE_FONT_FLASH
#endif

#endif
