/********************************************************************
 * FileName:		ACCEL_BMA150Config.h
 * Dependencies:
 * Processor:		PIC32MX
 * Hardware:		N/A
 * Assembler:		N/A
 * Linker:		    N/A
 * Company:		    Microchip Technology Inc..
 *
 * Software License Agreement:
 * The software supplied herewith by Microchip Technology Incorporated
 * (the �Company�) for its PICmicro� Microcontroller is intended and
 * supplied to you, the Company�s customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * $Id: INT.h,v 1.6 2006/11/07 23:29:45 C12923 Exp $
 * $Name:  $

 ********************************************************************/

#ifndef _ACCEL_BMA150_CONFIGURATION_H
#define _ACCEL_BMA150_CONFIGURATION_H

// *****************************************************************************
/* Accelerometer I2C Module Configuration.

  Summary:
    Accelerometer I2C Module configuration.

  Description:
	Communication to the Accelerometer device is through the I2C module.  The application must select 
	an I2C module.  The I2C module must be consistant for all macros.

  Remarks:
	None.
*/
#define ACCEL_BMA150_USE_I2C2
/*************************************************************************
  Summary:
    Accelerometer I2C Module Data Rate.
  Description:
    Accelerometer I2C Module Data Rate.
    
    The data rate that is used for communicating to the Accelerometer device.

  Remarks:
	None.
  *************************************************************************/
#define ACCEL_BMA150_BAUD          (400000)


//#define ACCEL_USE_POLLING
//#define ACCEL_USE_EXTERNAL_INTERUPT_HANDLER

#endif
